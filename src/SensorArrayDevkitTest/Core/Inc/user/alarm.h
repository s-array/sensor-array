#ifndef ALARM_H 
#define ALARM_H 

#include "config.h"
#include "alarmconfig.h"

#include "stm32wbxx.h"

/**
 * \brief Start and stop the alarm on the device
 */
class Alarm{
	public:
		/**
		 * \brief Constructor
		 *
		 * @param conf[in] Pointer to a Config object
		 * @param GPIOx[in] Register, such as GPIOB
		 * @param ledRPin[in] The pin on which the Red LED is connected
		 * @param buzzerPin[in] The pin on which the buzzer is connected
		 */
		Alarm(Config *conf, GPIO_TypeDef* GPIOx, uint16_t ledPin, uint16_t buzzerPin);
		
		/**
		 * \brief Load the configuration from the config in memory
		 */
		void loadConfig();
	
		/**
		 * \brief Start the alarm
		 */
		void start();

		/**
		 * \brief Stop the alarm
		 */
		void stop();

		/**
		 * \brief Tick the alarm
		 *
		 * Use Timer IRQ or threaded while loop with sleep
		 */
		void tick();


	private:
		uint16_t _buzzerPin;
		uint16_t _ledPin;
		GPIO_TypeDef* _GPIOx;

		bool _running;

		AlarmConfig _alarmConfig;
		Config *_config;
};

#endif
