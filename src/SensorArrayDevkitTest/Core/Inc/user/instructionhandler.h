#ifndef INSTRUCTION_HANDLER_H
#define INSTRUCTION_HANDLER_H


#include <user/uartpackets.h>
#include <user/rtcdevice.h>
#include <user/config.h>

#include "stm32wbxx_hal.h"

#include <cstdint>
#include <cstring>

#define UART_RX_BUFFER_SIZE sizeof(GetDeviceInformation_TX)

enum class StreamState {RUNNING, NOT_RUNNING};

/**
 * \brief Handler for UART instructions from the SALib
 */
class InstructionHandler{
	public:
		/**
		 * \brief Constructor
		 * @param config[in] Pointer to a Config object
		 * @param rtc[in] Pointer to a RTC object
		 * @param uart[in] Pointer to the UART_HandleTypeDef struct
		 */
		InstructionHandler(Config *config, RTCDevice *rtc, UART_HandleTypeDef *uart);

		/**
		 * \brief Deconstructor
		 */
		//~InstructionHandler();

		/**
		 * \brief Write stream data to UART
		 *
		 * Blocking function. Should be executed on a thread.
		 *
		 * @param value The value to send
		 */
		void sendStreamData(uint32_t value);

		/**
		 * \brief Get the current state of the stream
		 *
		 * @return The current StreamState
		 */
		StreamState getStreamState();
		
		/**
		 * \brief Execute a received UART instruction
		 *
		 * @param buff The instruction buffer to execute
		 */
		void executeInstruction(uint8_t *buff);

		/**
		 * \brief Get the size of a packet by the instruction id
		 *
		 * @param instructionId The instruction ID
		 * @return The size of the packet
		 */
		uint8_t getPacketSize(uint8_t instructionId);


	private:
		bool _canExecuteInstruction(uint8_t instructionId);
		uint8_t _getInstructionId(uint8_t *buff);
		bool _sendPacket(UARTPacket *p, size_t size);
		bool _sendACK();
		void _handleGetConfigTime();
		void _handleSetConfigTime(SetConfigTime_RX *rx);
		void _handleGetAlarmConfig();
		void _handleSetAlarmConfig(SetAlarmConfig_RX *rx);
		void _handleGetSensorThreshold(GetSensorThreshold_RX *rx);
		void _handleSetSensorThreshold(SetSensorThreshold_RX *rx);
		void _handleGetModuleConfig(GetModuleConfig_RX *rx);
		void _handleSetModuleConfig(SetModuleConfig_RX *rx);
		void _handleStartStream(StartStream_RX *rx);
		void _handleStopStream();
		void _handleFactoryReset();
		void _handleGetDeviceInformation();
	
		//Variables
		uint8_t _lastInstructionId;
		StreamState _streamState;

		//Objects
		Config *_config;
		RTCDevice *_rtc;
		UART_HandleTypeDef *_uart;
		
};

#endif
