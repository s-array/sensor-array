#ifndef STORAGEHANDLER_H 
#define STORAGEHANDLER_H 

#include <stddef.h>
#include <FreeRTOS.h>
#include <semphr.h>

#define ADDR_CONFIG_BASE 0x0


/**
 * \brief Handler for data storage
 */
class StorageHandler{
	public:
		/**
		 * \brief Constructor
		 */
		StorageHandler();
		
		/**
		 * \brief Deconstructor
		 */
		~StorageHandler();
		
		/**
		 * \brief Write data
		 *
		 * @param addr[in] The address to write to
		 * @param buff[in] The data to write
		 * @param size[in] The amount of bytes to write
		 * @return Writing data was successful
		 */
		bool write(uint32_t addr, void *buff, size_t size);

		/**
		 * \brief Read data
		 *
		 * @param addr[in] The address to read from
		 * @param buff[out] The output data after reading
		 * @param size[in] The amount of bytes to read
		 * @return Reading data was successful
		 */
		bool read(uint32_t addr, void *buff, size_t size);
	
#ifdef DEBUG
		void printMem();
#endif



	private:
		/**
		 * \brief Check if the address is in range
		 *
		 * @param addr[in] The address to check
		 * @return Address is in range
		 */
		bool isInRange(uint32_t addr);
		

		//TODO: remove this
		uint8_t *_simulatedMemory;

		//Variables
		SemaphoreHandle_t _rwMutex;

};

#endif
