#ifndef RTCDEVICE_H
#define RTCDEVICE_H

#include <cstdint>

/**
 * \brief Realtime clock HAL class
 */
class RTCDevice{
	public:
		/**
		 * \brief Constructor
		 */
		RTCDevice();

		/**
		 * \brief Deconstructor 
		 */
		~RTCDevice();
		
		/**
		 * \brief Get the unix epoch time in seconds
		 *
		 * @return Time in seconds
		 */
		uint32_t getTimestamp();

		/**
		 * \brief Update the epoch time on the RTC
		 *
		 * @return Update was successful
		 */
		bool setTimestamp(uint32_t timestamp);
};

#endif
