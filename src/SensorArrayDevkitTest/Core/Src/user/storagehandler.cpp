#include "user/storagehandler.h"
#include <cstring>
#include <cstdlib>

#define MEMORY_SIZE 5

StorageHandler::StorageHandler(){
	//this->_rwMutex = xSemaphoreCreateMutex();

	//TODO: Remove simulated memory
	this->_simulatedMemory = (uint8_t *)malloc(MEMORY_SIZE);
}

StorageHandler::~StorageHandler(){
	//TODO: Remove simulated memory
	free(this->_simulatedMemory);
}

bool StorageHandler::write(uint32_t addr, void *buff, size_t size){
	if(!this->isInRange(addr)){
		return false;
	}
	
	//Write to memory
	//xQueueSemaphoreTake(this->_rwMutex, portMAX_DELAY);
	memcpy((this->_simulatedMemory + addr), buff, size);
	//xSemaphoreGive(&this->_rwMutex);

	return true;
}

bool StorageHandler::read(uint32_t addr, void *buff, size_t size){
	if(!this->isInRange(addr)){
		return false;
	}

	//Read from memory
	//xQueueSemaphoreTake(this->_rwMutex, portMAX_DELAY);
	memcpy(buff, (this->_simulatedMemory + addr), size);
	//xSemaphoreGive(&this->_rwMutex);

	return true;
}

bool StorageHandler::isInRange(uint32_t addr){
	return (addr <= MEMORY_SIZE-1);
}
