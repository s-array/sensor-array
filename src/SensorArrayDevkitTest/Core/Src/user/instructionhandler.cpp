#include <user/instructionhandler.h>
#include <iostream>
using namespace std;

InstructionHandler::InstructionHandler(Config *config, RTCDevice *rtc, UART_HandleTypeDef *uart):
	_config(config),
	_rtc(rtc),
	_uart(uart)
{
	this->_streamState = StreamState::NOT_RUNNING;
}

/*void InstructionHandler::receiveData(uint8_t data){

	this->_rxBuffer[this->_rxBufferSize] = data;
	this->_rxBufferSize++;

	if(this->_rxBufferSize == this->_getPacketSize(this->_getInstructionId(this->_rxBuffer))){
		//Clear the buffer
		this->_rxBufferSize = 0;

		//Execute the received instruction
		this->_executeInstruction();
	}
}*/

void InstructionHandler::sendStreamData(uint32_t value){
	StreamData_TX sd;
	sd.timestamp = this->_rtc->getTimestamp();

	//Read from sensor
	sd.sensorModuleId = MODULE_ID;
	sd.sensorType = 0;	//TODO
	sd.sensorValue = value;

	this->_sendPacket(&sd, sizeof(sd));
}

StreamState InstructionHandler::getStreamState(){
	return this->_streamState;
}

void InstructionHandler::executeInstruction(uint8_t *buff){
	this->_lastInstructionId = this->_getInstructionId(buff);
	
	//Check if instructions can be executed
	if(!this->_canExecuteInstruction(this->_lastInstructionId)){
		return;
	}
	
	//Execute the instruction
	switch(this->_lastInstructionId){
		case 0x10:
			this->_handleGetConfigTime();
			break;
		case 0x11:
			this->_handleSetConfigTime((SetConfigTime_RX *)buff);
			break;
		case 0x12:
			this->_handleGetAlarmConfig();
			break;
		case 0x13:
			this->_handleSetAlarmConfig((SetAlarmConfig_RX *)buff);
			break;
		case 0x14:
			this->_handleGetSensorThreshold((GetSensorThreshold_RX *)buff);
			break;
		case 0x15:
			this->_handleSetSensorThreshold((SetSensorThreshold_RX *)buff);
			break;
		case 0x16:
			this->_handleGetModuleConfig((GetModuleConfig_RX *)buff);
			break;
		case 0x17:
			this->_handleSetModuleConfig((SetModuleConfig_RX *)buff);
			break;
		case 0x20:
			this->_handleStartStream((StartStream_RX *)buff);
			break;
		case UART_PACKETID_STOP_STREAM:
			this->_handleStopStream();
			break;
		case 0xFE:
			this->_handleFactoryReset();
			break;
		case 0xFF:
			this->_handleGetDeviceInformation();
			break;
		default:
			break;
	}
}

uint8_t InstructionHandler::getPacketSize(uint8_t instructionId){
	switch(instructionId){
		case 0x01:
			return sizeof(UARTInstructionPacket);
		case 0x02:
			return sizeof(EventDelete_RX);
		case 0x03:
			return sizeof(UARTInstructionPacket);
		case 0x10:
			return sizeof(UARTInstructionPacket);
		case 0x11:
			return sizeof(SetConfigTime_RX);
		case 0x12:
			return sizeof(UARTInstructionPacket);
		case 0x13:
			return sizeof(SetAlarmConfig_RX);
		case 0x14:
			return sizeof(GetSensorThreshold_RX);
		case 0x15:
			return sizeof(SetSensorThreshold_RX);
		case 0x16:
			return sizeof(GetModuleConfig_RX);
		case 0x17:
			return sizeof(SetModuleConfig_RX);
		case 0x20:
			return sizeof(StartStream_RX);
		case UART_PACKETID_STOP_STREAM:
			return sizeof(UARTInstructionPacket);
		case 0xFE:
			return sizeof(UARTInstructionPacket);
		case 0xFF:
			return sizeof(UARTInstructionPacket);
		default:
			break;
	}
	return 0;
}

bool InstructionHandler::_canExecuteInstruction(uint8_t instructionId){
	if(instructionId == UART_PACKETID_STOP_STREAM){
		//Instruction can always be executed when the stream should be stopped
		return true;
	}

	return !(this->_streamState == StreamState::RUNNING);
}


uint8_t InstructionHandler::_getInstructionId(uint8_t *buff){
	return (uint8_t)buff[0];
}


bool InstructionHandler::_sendPacket(UARTPacket *p, size_t size){
	HAL_StatusTypeDef status;
	status = HAL_UART_Transmit(this->_uart, (uint8_t *)p, size, portMAX_DELAY);
	return status == HAL_OK;
}

bool InstructionHandler::_sendACK(){
	UARTInstructionPacket p(this->_lastInstructionId);
	return this->_sendPacket(&p, sizeof(p));
}

void InstructionHandler::_handleGetConfigTime(){
	GetConfigTime_TX p;

	p.timestamp = this->_rtc->getTimestamp();

	//Send response
	this->_sendPacket(&p, sizeof(p));
}

void InstructionHandler::_handleSetConfigTime(SetConfigTime_RX *rx){
	if(this->_rtc->setTimestamp(rx->timestamp)){
		this->_sendACK();
	}
}

void InstructionHandler::_handleGetAlarmConfig(){
	GetAlarmConfig_TX p;
	
	//Get the alarm config struct
	AlarmConfig alarmConf = this->_config->getAlarmConfig();
	
	//Convert the struct to uint8 and send the packet
	p.alarmConfig = alarmConf.to_uint8();
	this->_sendPacket(&p, sizeof(p));
}

void InstructionHandler::_handleSetAlarmConfig(SetAlarmConfig_RX *rx){
	//Cast uint8_t to alarm config struct
	AlarmConfig alarmConf = AlarmConfig(rx->alarmConfig);
	
	//Update the config
	if(this->_config->setAlarmConfig(alarmConf)){
		//Send ACK if succeeded
		this->_sendACK();
	}
}

void InstructionHandler::_handleGetSensorThreshold(GetSensorThreshold_RX *rx){
	GetSensorThreshold_TX p;

	//Get sensor type from struct
	p.value = this->_config->getThreshold();

	//Send response
	this->_sendPacket(&p, sizeof(p));
}


void InstructionHandler::_handleSetSensorThreshold(SetSensorThreshold_RX *rx){
	//Set the sensor threshold
	if(this->_config->setThreshold(rx->thresholdValue)){

		//Send ACK if succeeded
		this->_sendACK();
	}
}

void InstructionHandler::_handleGetModuleConfig(GetModuleConfig_RX *rx){
	GetModuleConfig_TX p;

	p.sensorModuleId = rx->sensorModuleId;
	p.alarmConfig = this->_config->getAlarmConfig().to_uint8();
	p.sensorType = 0;	//TODO
	
	//Send response
	this->_sendPacket(&p, sizeof(p));
}

void InstructionHandler::_handleSetModuleConfig(SetModuleConfig_RX *rx){
	SetModuleConfig_TX p;

	//Update the alarm config. The device will be synced
	p.status = 1;
	this->_config->setAlarmConfig(rx->alarmConfig);
	
	//Send response
	p.sensorModuleId = p.sensorModuleId = rx->sensorModuleId;;
	this->_sendPacket(&p, sizeof(p));
}

void InstructionHandler::_handleStartStream(StartStream_RX *rx){
	//Start the stream
	this->_streamState = StreamState::RUNNING;

	//Send the ACK packet
	this->_sendACK();
}

void InstructionHandler::_handleStopStream(){
	//Stop the stream
	this->_streamState = StreamState::NOT_RUNNING;

	//Send the ACK
	this->_sendACK();
}

void InstructionHandler::_handleFactoryReset(){
	//Perform factory resets
	if(!this->_config->restoreFactoryDefaults()){
		return;
	}
	
	//Send the ACK
	this->_sendACK();

	//TODO: reboot the device to clear all queues and restart all threads
}

void InstructionHandler::_handleGetDeviceInformation(){
	GetDeviceInformation_TX p;
	this->_sendPacket(&p, sizeof(p));
}
