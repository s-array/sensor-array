#include "user/config.h"

#define ADDR_ALARM_BASE 0
#define ADDR_THRESHOLD_BASE 1

Config::Config(StorageHandler *storage):
	_storage(storage)
{
	//TODO
}

Config::~Config(){
	//TODO
}

bool Config::setThreshold(uint32_t value){
	//Write the threshold to the storage
	uint32_t addr = ADDR_CONFIG_BASE + ADDR_THRESHOLD_BASE;
	return this->_storage->write(addr, &value, sizeof(value));
}

uint32_t Config::getThreshold(){
	uint32_t value = 0;
	uint32_t addr = ADDR_CONFIG_BASE + ADDR_THRESHOLD_BASE;
		
	//Read the threshold
	this->_storage->read(addr, &value, sizeof(value));
	
	//Return the threshold
	return value;

}

bool Config::setAlarmConfig(AlarmConfig config){
	//Store the alarm config in memory
	uint8_t data = config.to_uint8();
	return this->_storage->write(ADDR_CONFIG_BASE + ADDR_ALARM_BASE, &data, sizeof(data));
}

AlarmConfig Config::getAlarmConfig(){
	//Get the alarm config from memory
	uint8_t data = 0;
	if(!this->_storage->read(ADDR_CONFIG_BASE + ADDR_ALARM_BASE, &data, sizeof(data))){
		//Enable all alarms if reading is not possible
		data = 0xFF;
	}
	
	//Return the alarm config struct
	return AlarmConfig(data);
}

bool Config::restoreFactoryDefaults(){
	AlarmConfig ac;

	//Reset the alarm config
	ac.ledEnabled = true;
	ac.buzzerEnabled = true;
	if(!this->setAlarmConfig(ac)){
		return false;
	}

	//Clear all thresholds
	if(!this->setThreshold(0)){
		return false;
	}

	return true;
}
