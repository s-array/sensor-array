#include "user/alarm.h"

#define LED_BLINK_PERIOD 5
#define BUZZER_FREQ_BIAS 250
#define BUZZER_FREQ_MULTIPLIER 10

#include <iostream>
using namespace std;

Alarm::Alarm(Config *conf, GPIO_TypeDef* GPIOx, uint16_t ledPin, uint16_t buzzerPin):
	_config(conf),
	_GPIOx(GPIOx),
	_ledPin(ledPin),
	_buzzerPin(buzzerPin)
{
	//Enable all alarms (Can be disabled by reading from the config)
	this->_alarmConfig = AlarmConfig(0xFF);
	this->_running = false;
}

void Alarm::loadConfig(){
	this->_alarmConfig = this->_config->getAlarmConfig();
}

void Alarm::start(){
	this->_running = true;
}

void Alarm::stop(){
	this->_running = false;

	HAL_GPIO_WritePin(this->_GPIOx, this->_ledPin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(this->_GPIOx, this->_buzzerPin, GPIO_PIN_RESET);
}

void Alarm::tick(){
	if(!this->_running){
		return;
	}

	if(this->_alarmConfig.ledEnabled){
		HAL_GPIO_TogglePin(this->_GPIOx, this->_ledPin);
	}

	if(this->_alarmConfig.buzzerEnabled){
		HAL_GPIO_TogglePin(this->_GPIOx, this->_buzzerPin);
	}
}
