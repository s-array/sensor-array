# Library

## Installing requirements
`` pip install -r requirements.txt ``

## Running the application
`` cd src/cli/ ``
`` python src/cli/cli_salib.py ``

## Example code for library (SALib)
```
import sys
sys.path.insert(0, "path to salib folder")

from salib import *

# Initialize the library and connect to serial port
lib = SALib()
lib.openSerial("/dev/ttyUSB0")


# Check if the connected device is a mainhub or a module
print(lib.isMainhub())

# Sync the time on the mainhub with this device
lib.syncTime()

# Get all events from the mainhub
print(lib.getEvents())

# Get all known modules from the mainhub
print(lib.getModulesList())


# Check if a stream is already running
print(lib.dataStreamIsRunning())

# Start a stream and collect 10 samples
samples = list()

lib.startStream(5)	# Start stream for device with id 5
for i in range(0, 10):
	data - lib.readDataStream()	
	if(data != None):
		samples.append(data)

lib.stopDataStream() # Stop the stream
print("Samples collected: " + str(samples))

# Close the serial port
lib.closeSerial()
```

More functions are available. See 'src/salib/salib.py'.
