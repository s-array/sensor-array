
EXAMPLES

    	cli_salib.py --port COM1 --get device-info:4
    	cli_salib.py --port COM3 --measure start:output.txt:3:90
	cli_salib.py --port COM1 --d 33
	cli_salib.py --port /dev/ttyUSB0 --d 33

COMMANDS

    -p*, --port <selected port>

	A port must always be specified when running the CLI. The <selected port> refers to the
	serial port on the operation system.

    -y, --sync-time

	Sync the time between the hub module and the host computer.

    -g, --get <events>
              <modules>
              <alarm-config>
              <thresholds> <sensor type>
              <module-config>
              <device-info> <device-id>

	Get corosponding information. where
	  - events: A bulk of the measured data events from the modules sensors.
	  - modules: The connected sensor modules and their ID's.
	  - alarm-config: Configuration of the led and buzzers on the modules
	  - thresholds: The connected sensor modules and their set thresholds for alarms.
	    sensortype can be on of the following types: mq2, mq3, mq4, mq5, mq6, mq7, mq8, mq135
	  - module-config: TODO
	  - device-info: Get information of a specific device by ID.


    -s, --set <alarm-config> <led True/False> <buzz True/False>
              <module-config> <device-id> <alarm True/False><led True/False>
	      <threshold> <sensortype> <value>

	Set alarm configuration for a specific device.
	sensortype can be on of the following types: mq2, mq3, mq4, mq5, mq6, mq7, mq8, mq135

    -d, -delete <event-id>

	Delete an event by <event-id> from memory on the main hub.

    -m, -measure <output_file> <module_id> <duration:seconds>

	Start reading streaming data from a sensor module and write it to a csv file.
	If no module id is given, data from all modules will be collected.
	If the duration is 0 or lower, than the application will run indefinitely.

