import sys
sys.path.insert(0, "../")

from salib import *
import time

lib = SALib()
lib.openSerial("/dev/ttyACM0")



def testFunction(function, args=None):
    print("Testing: " + str(function))

    #Execute the function
    if(args != None):
        response = function(*tuple(args))
    else:
        response = function()

    print("Function returned: " + str(response))
    print("")




#Test for: isMainhub()
testFunction(lib.isMainhub)

if(lib.isMainhub()):

    #Test for: getEvents()
    testFunction(lib.getEvents)

    #Test for: deleteEvent()
    testFunction(lib.deleteEvent,[5])

    #Test for: deleteAllEvents()
    testFunction(lib.deleteAllEvents)

    #Test for: getModulesList()
    testFunction(lib.getModulesList)

    #Test for: getAlarmConfig()
    testFunction(lib.getAlarmConfig)

    #Test for: setAlarmConfig()
    testFunction(lib.setAlarmConfig, [False, True])

    #Test for: getSensorThreshold()
    testFunction(lib.getSensorThreshold, [5])

    #Test for: setSensorThreshold()
    testFunction(lib.setSensorThreshold, [5, 56789])


#Test for: syncTime()
testFunction(lib.syncTime)

#Test for: getModuleConfig()
testFunction(lib.getModuleConfig,[0xA241])

#Test for: setModuleConfig()
testFunction(lib.setModuleConfig, [0, True, True])

#Test for: startDataStream()
testFunction(lib.startDataStream)

#Test for: dataStreamIsRunning()
testFunction(lib.dataStreamIsRunning)

#Test for: readDataStream()
dataList = list()
for i in range(0, 10):
    data = lib.readDataStream()
    if(data != None):
        dataList.append(data)

print("Data stream list:" + str(dataList) + "\n")
        
#Test for: stopDataStream()
testFunction(lib.stopDataStream)

#Test for: dataStreamIsRunning()
testFunction(lib.dataStreamIsRunning)

#Test for: getDeviceInformation()
testFunction(lib.getDeviceInformation)

#Test for: factoryReset()
testFunction(lib.factoryReset)




#Results:
#Testing: <bound method SALib.isMainhub of <salib.salib.SALib object at 0x7fc311500fd0>>
#Writing data: b'\xff'
#Data received(34): b'\xff1.0\x00\x00\x00\x00\x00\x00\x00\x00Jan 18 202115:59:25\x00\x01\x00'
#Function returned: False
#
#Testing: <bound method SALib.syncTime of <salib.salib.SALib object at 0x7fc311500fd0>>
#Writing data: b'\x10'
#Data received(5): b'\x10\xff\xff\xff\xff'
#Writing data: b'\x11\xf5\xa2\x05`'
#Data received(1): b'\x11'
#Function returned: True
#
#Testing: <bound method SALib.getModuleConfig of <salib.salib.SALib object at 0x7fc311500fd0>>
#Writing data: b'\x16A\xa2'
#Data received(5): b'\x16A\xa2\x03\x00'
#Function returned: {'module_id': 41537, 'sensor_type': 0, 'alarm': {'led_enabled': True, 'buzzer_enabled': True}}
#
#Testing: <bound method SALib.setModuleConfig of <salib.salib.SALib object at 0x7fc311500fd0>>
#Writing data: b'\x17\x00\x00\x03'
#Data received(4): b'\x17\x00\x00\x01'
#Function returned: True
#
#Testing: <bound method SALib.startDataStream of <salib.salib.SALib object at 0x7fc311500fd0>>
#Writing data: b' \xff\xff'
#Data received(1): b' '
#Function returned: True
#
#Testing: <bound method SALib.dataStreamIsRunning of <salib.salib.SALib object at 0x7fc311500fd0>>
#Function returned: True
#
#Data received(11): b'\xff\xff\xff\xff\x01\x00\x00\xc8\x00\x00\x00'
#Data received(11): b'\xff\xff\xff\xff\x01\x00\x00\xc8\x00\x00\x00'
#Data received(11): b'\xff\xff\xff\xff\x01\x00\x00\xc8\x00\x00\x00'
#Data received(11): b'\xff\xff\xff\xff\x01\x00\x00\xc8\x00\x00\x00'
#Data received(11): b'\xff\xff\xff\xff\x01\x00\x00\xc8\x00\x00\x00'
#Data received(11): b'\xff\xff\xff\xff\x01\x00\x00\xc8\x00\x00\x00'
#Data received(11): b'\xff\xff\xff\xff\x01\x00\x00\xc8\x00\x00\x00'
#Data received(11): b'\xff\xff\xff\xff\x01\x00\x00\xc8\x00\x00\x00'
#Data received(11): b'\xff\xff\xff\xff\x01\x00\x00\xc8\x00\x00\x00'
#Data received(11): b'\xff\xff\xff\xff\x01\x00\x00\xc8\x00\x00\x00'
#Data stream list:[{'timestamp': 4294967295, 'module_id': 1, 'sensor_type': 0, 'sensor_value': 200}, {'timestamp': 4294967295, 'module_id': 1, 'sensor_type': 0, 'sensor_value': 200}, {'timestamp': 4294967295, 'module_id': 1, 'sensor_type': 0, 'sensor_value': 200}, {'timestamp': 4294967295, 'module_id': 1, 'sensor_type': 0, 'sensor_value': 200}, {'timestamp': 4294967295, 'module_id': 1, 'sensor_type': 0, 'sensor_value': 200}, {'timestamp': 4294967295, 'module_id': 1, 'sensor_type': 0, 'sensor_value': 200}, {'timestamp': 4294967295, 'module_id': 1, 'sensor_type': 0, 'sensor_value': 200}, {'timestamp': 4294967295, 'module_id': 1, 'sensor_type': 0, 'sensor_value': 200}, {'timestamp': 4294967295, 'module_id': 1, 'sensor_type': 0, 'sensor_value': 200}, {'timestamp': 4294967295, 'module_id': 1, 'sensor_type': 0, 'sensor_value': 200}]
#
#Testing: <bound method SALib.stopDataStream of <salib.salib.SALib object at 0x7fc311500fd0>>
#Writing data: b'!'
#Data received(1): b'!'
#Function returned: True
#
#Testing: <bound method SALib.dataStreamIsRunning of <salib.salib.SALib object at 0x7fc311500fd0>>
#Function returned: False
#
#Testing: <bound method SALib.getDeviceInformation of <salib.salib.SALib object at 0x7fc311500fd0>>
#Function returned: {'software': {'version_number': '1.0\x00\x00\x00\x00\x00\x00\x00\x00', 'compile_date': 'Jan 18 2021', 'compile_time': '15:59:25'}, 'device_type': 0, 'module_id': 1}
#
#Testing: <bound method SALib.factoryReset of <salib.salib.SALib object at 0x7fc311500fd0>>
#Writing data: b'\xfe'
#Data received(1): b'\xfe'
#Function returned: True
