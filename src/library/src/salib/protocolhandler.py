import serial
from time import sleep
import struct
from .packettypes import *
import time

class ProtocolHandler:
    BAUDRATE = 115200
    BYTESIZE = serial.EIGHTBITS
    PARITY = serial.PARITY_NONE
    STOPBITS = serial.STOPBITS_ONE
    TIMEOUT = 10
    ENDIANNESS_CHAR = "<"

    _serial = None




    def __init__(self):
        pass




    def transmitPacket(self, packetTypeStr, data=list()):
        #Get packet information
        packet = getPacketType(packetTypeStr)

        #Create packet data tuple
        packetData = struct.pack(self.ENDIANNESS_CHAR + packet["uplink"]["format"], packet["id"], *tuple(data))
        
        #Write the packet
        return self._writeData(packetData)

   


    def getResponse(self, packetTypeStr):
        #Get packet information
        packet = getPacketType(packetTypeStr)
        packetSize = packet["downlink"]["size"]
        
        #Read the data
        rawData = self._readData(packetSize)
        if(rawData == None):
            return None
        else:
            data = struct.unpack(self.ENDIANNESS_CHAR + packet["downlink"]["format"], rawData)
    
        #Check if the packet ID matches
        if(data[0] != packet["id"] and packet["id"] != None):
            raise Exception("The received instruction ID does not match the expected instruction ID.")
            return None
        
        data = list(data)
        #Convert bytes to strings
        for i, value in enumerate(data):
            if type(value) == type(bytes()):
                data[i] = value.decode("utf-8")

        if(packet["id"] == None):
            #Packet has no ID, return all data
            return tuple(data)
        else:
            #Packet has an ID, return the other data
            return tuple(data[1:])




    def openSerial(self, portName):
        try:
            self._serial = serial.Serial(port=portName,
                                        baudrate=self.BAUDRATE,
                                        parity=self.PARITY,
                                        stopbits=self.STOPBITS,
            )

            #time.sleep(2)

            # self._serial.setDTR(False)
            # print('dtr ='), se.dtr
            #self._serial.close()
            #self._serial.open()
            sleep(2)
            return True
        except:
            return False 




    def closeSerial(self):
        return self._serial.close()




    def _serialIsOpen(self):
        return (self._serial != None)




    def _readData(self, n):
        if not self._serialIsOpen():
            return None
        
        data = self._serial.read(n)
        print("Data received(" + str(len(data)) + "): " + str(data))
        return data




    def _writeData(self, data):
        if not self._serialIsOpen():
            return False
         
        print("Writing data: " + str(data))
        return self._serial.write(data) > 0




    def resetRXBuffer(self):
        self._serial.flushInput()
