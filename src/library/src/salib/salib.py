from .protocolhandler import *
import time

class SALib():
    _protocol = None
    _dataStreamRunning = False
    _deviceInfo = None




    def __init__(self):
        self._protocol = ProtocolHandler()
    


    def openSerial(self, serialPort):
        return self._protocol.openSerial(serialPort)



    def closeSerial(self):
        return self._protocol.closeSerial();



    def getEvents(self):
        #Check if device is a module
        if not self.isMainhub():
            self._functionNotAvailableException()

        if not self._protocol.transmitPacket("READ_EVENT"):
            return None
        
        #Read the amount of events stored on the device
        eventCount = self._protocol.getResponse("READ_EVENT")[0]
        events = list()
    
        for i in range(0, eventCount):
            events.append(self._protocol.getResponse("READ_EVENT_DATA"))
            
        return events

    def deleteEvent(self, n):
        #Check if device is a module
        if not self.isMainhub():
            self._functionNotAvailableException()

        if not self._protocol.transmitPacket("DELETE_EVENT", [n]):
            return None
        
        #Read the amount of events stored on the device
        deletedID = self._protocol.getResponse("DELETE_EVENT")[0]
        return deletedID == n



    def deleteAllEvents(self):
        return self.deleteEvent(0xFFFF)



    def getModulesList(self):
        #Check if device is a module
        if not self.isMainhub():
            self._functionNotAvailableException()

        if not self._protocol.transmitPacket("GET_MODULES_LIST"):
            return None
        
        #Read the amount of modules stored on the device
        moduleCount = self._protocol.getResponse("GET_MODULES_LIST")[0]
        modules = list()
    
        for i in range(0, moduleCount):
            moduleID = self._protocol.getResponse("GET_MODULES_LIST_DATA")
            modules.append(moduleID[0])
            
        return modules 



    def getAlarmConfig(self):
        #Check if device is a module
        if not self.isMainhub():
            self._functionNotAvailableException()

        if not self._protocol.transmitPacket("GET_ALARM_CONFIG"):
            return None
        
        #Read the alarm config
        alarmConfig = self._protocol.getResponse("GET_ALARM_CONFIG")[0]
        if(alarmConfig == None):
            return None

        conf = {
            "led_enabled": bool(alarmConfig & 0x01),
            "buzzer_enabled": bool(alarmConfig & 0x02)
        }
        return conf




    def setAlarmConfig(self, ledEnabled, buzzerEnabled):
        #Check if device is a module
        if not self.isMainhub():
            self._functionNotAvailableException()


        data = int(0)
        data = data | int(ledEnabled)
        data = data | (int(buzzerEnabled) << 1)


        if not self._protocol.transmitPacket("SET_ALARM_CONFIG", [data]):
            return False
        
        #Read the amount of events stored on the device
        return self._protocol.getResponse("SET_ALARM_CONFIG") != None



    def getSensorThreshold(self, sensorType):
        #Check if device is a module
        if not self.isMainhub():
            self._functionNotAvailableException()

        if not self._protocol.transmitPacket("GET_SENSOR_THRESHOLD", [sensorType]):
            return None
        
        #Read the threshold 
        return self._protocol.getResponse("GET_SENSOR_THRESHOLD")[0]



    def setSensorThreshold(self, sensorType, value):
        #Check if device is a module
        if not self.isMainhub():
            self._functionNotAvailableException()

        if not self._protocol.transmitPacket("SET_SENSOR_THRESHOLD", [sensorType, value]):
            return False
        
        #Read the response
        return self._protocol.getResponse("SET_SENSOR_THRESHOLD") != None




    def getModuleConfig(self, moduleId):
        if not self._protocol.transmitPacket("GET_MODULE_CONFIG", [moduleId]):
            return None
        
        #Read the response
        response = self._protocol.getResponse("GET_MODULE_CONFIG")
        if(response == None):
            return None
        
        #Create the struct
        conf = {
            "module_id": response[0],
            "sensor_type": response[2],
            "alarm": {
                "led_enabled": bool(response[1] & 0x01),
                "buzzer_enabled": bool(response[1] & 0x02)
            }
        }

        #Throw an exception if the module id does not match
        if(conf["module_id"] != moduleId):
            raise Exception("The received module ID does not match the expected module ID.")
            return None
        else:
            return conf






    def setModuleConfig(self, moduleId, buzzerEnabled, ledEnabled):
        data = int(0)
        data = data | int(ledEnabled)
        data = data | (int(buzzerEnabled) << 1)

        if not self._protocol.transmitPacket("SET_MODULE_CONFIG", [moduleId, data]):
            return False

        #Read the response
        response = self._protocol.getResponse("SET_MODULE_CONFIG")
        if(response == None):
            return False

        if(response[0] != moduleId):
            raise Exception("The received module ID does not match the expected module ID.")
            return False
        else:
            return bool(response[1])






    def startDataStream(self, moduleId=0xFFFF):
        if not self._protocol.transmitPacket("START_READING_DATA", [moduleId]):
            return False

        #Read the response
        response = self._protocol.getResponse("START_READING_DATA")
        if(response == None):
            return False

        self._dataStreamRunning = True
        return True

    


    def stopDataStream(self):
        if not self._protocol.transmitPacket("STOP_READING_DATA"):
            return False

        #Reset the input buffer
        #TODO: test this
        #self._protocol.resetRXBuffer()

        #Read the response
        response = self._protocol.getResponse("STOP_READING_DATA")
        if(response == None):
            return False
        

        self._dataStreamRunning = False
        return True
    



    def dataStreamIsRunning(self):
        return self._dataStreamRunning




    def readDataStream(self):
        if not self.dataStreamIsRunning():
            return None
        
        #Read data from the stream
        response = self._protocol.getResponse("READING_DATA_STREAM")
        if(response == None):
            return None
        
        #Fill data dict
        data = {
            "timestamp": response[0],
            "module_id": response[1],
            "sensor_type": response[2],
            "sensor_value": response[3]
        }
        return data




    def getDeviceInformation(self):
        if(self._deviceInfo == None):
            if not self._protocol.transmitPacket("GET_DEVICE_INFO"):
                return None
            
            #Read the response
            response = self._protocol.getResponse("GET_DEVICE_INFO")
            if(response == None):
                return None
            
            #Fill the info struct
            self._deviceInfo = {
                "software": {
                    "version_number": response[0],
                    "compile_date": response[1],
                    "compile_time": response[2]
                },
                "device_type": response[3],
                "module_id": response[4]
            }


        return self._deviceInfo




    def syncTime(self):
        if not self._protocol.transmitPacket("GET_TIME"):
            return False
        
        timeResponse = self._protocol.getResponse("GET_TIME")[0]
        if(timeResponse == None):
            return False
        
        if(timeResponse >= int(time.time() + 10) and timeResponse <= int(time.time() - 10)):
            #Time is already synced
            return True
        else:
            #Set the time
            if not self._protocol.transmitPacket("SET_TIME", [int(time.time())]):
                return False
            
            return self._protocol.getResponse("SET_TIME") != None



    def factoryReset(self):
        if not self._protocol.transmitPacket("FACTORY_RESET"):
            return False
        else:
            return (self._protocol.getResponse("FACTORY_RESET") != None)




    def _getDeviceType(self):
        return self.getDeviceInformation()["device_type"]




    def isMainhub(self):
        return self._getDeviceType() == 0x01




    def _functionNotAvailableException(self):
        raise Exception("Function is not available on the module. Connect to the mainhub to use this function.")
