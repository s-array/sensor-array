import sys
sys.path.insert(0, "../")

from salib import * 

class UnitTestProtocolHandler(ProtocolHandler):
    _availableData = b''
    _writtenData = b''

    def setAvailableData(self, data):
        self._availableData = data
    
    def _readData(self, n):
        data = self._availableData[0:n]
        self._availableData = self._availableData[n:]
        return data

    def _writeData(self, data):
        self._writtenData = data
        return True

    def getWrittenData(self):
        return self._writtenData 
