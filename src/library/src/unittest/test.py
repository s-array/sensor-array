import sys
sys.path.insert(0, "../")

from salib import *
from unittestprotocolhandler import * 
import time

lib = SALib()
lib._protocol = UnitTestProtocolHandler()

def testFunction(function, RXBuffer, TXData, args=None):
    #Set the RX buffer data
    lib._protocol.setAvailableData(RXBuffer)
    
    #Execute the function
    if(args != None):
        response = function(*tuple(args))
    else:
        response = function()

    #Compare the transmitted data to the expected data
    txEquals = (TXData == lib._protocol.getWrittenData())
    
    print("Testing: " + str(function))
    print("Function returned: " + str(response))
    if(TXData != None):
        print("TX data test passed: " + str(txEquals))
        if not txEquals:
            print("TX data: " + str(lib._protocol.getWrittenData()))
    print("")





#Test device:
DEVICE_TYPE = 1
MODULE_ID = 523


rx = struct.pack("B", 0xFE)
tx = struct.pack("B", 0xFE)
testFunction(lib.factoryReset, rx, tx)



#Test for: isMainhub()
rx = struct.pack("<B11s11s8sBH", 255, b'1.2.3.4.5.6', b'Jan 01 2020', b'14:30:05', DEVICE_TYPE, MODULE_ID)
tx = struct.pack("<B", 0xFF)
testFunction(lib.isMainhub, rx, tx)



#Test for: getEvents()
#10 Devices in total
rx = struct.pack("<BH", 1, 10)

for i in range(0, 10):
    eventId = i
    sensorModuleID = 4 + i * 2
    unixTimestamp = int(time.time() + i)
    sensorValue = (i+1) * 1234

    rx = rx + struct.pack("<HHII", eventId, sensorModuleID, unixTimestamp, sensorValue)
testFunction(lib.getEvents, rx, b'\x01')




#Test for: deleteEvent()
rx = struct.pack("<BH", 2, 5)
testFunction(lib.deleteEvent, rx, b'\x02\x05\x00', [5])




#Test for: deleteAllEvents()
rx = struct.pack("<BH", 2, 0xFFFF)
testFunction(lib.deleteAllEvents, rx, b'\x02\xFF\xFF')




#Test for: getModulesList()
#10 Modules in total
rx = struct.pack("<BH", 3, 10)
tx = b'\x03'

for i in range(0, 10):
    sensorModuleID = 4 + i * 2
    rx = rx + struct.pack("<H", sensorModuleID)

testFunction(lib.getModulesList, rx, tx)




#Test for: syncTime()
rx = struct.pack("<BI", 0x10, int(time.time()))
rx = rx + struct.pack("<B", 0x11)
testFunction(lib.syncTime, rx, None)




#Test for: getAlarmConfig()
rx = struct.pack("<BB", 0x12, 0x02)
tx = struct.pack("<B", 0x12)
testFunction(lib.getAlarmConfig, rx, tx)




#Test for: setAlarmConfig()
rx = struct.pack("<B", 0x13)
tx = struct.pack("<BB", 0x13, 2)
testFunction(lib.setAlarmConfig, rx, tx, [False, True])




#Test for: getSensorThreshold()
rx = struct.pack("<BI", 0x14, 12345)
tx = struct.pack("<BB", 0x14, 5)
testFunction(lib.getSensorThreshold, rx, tx, [5])




#Test for: setSensorThreshold()
rx = struct.pack("<B", 0x15)
tx = struct.pack("<BBI", 0x15, 5, 56789)
testFunction(lib.setSensorThreshold, rx, tx, [5, 56789])




#Test for: getModuleConfig()
rx = struct.pack("<BHBB", 0x16, 0xA241, 0x03, 0x02)
tx = struct.pack("<BH", 0x16, 0xA241)
testFunction(lib.getModuleConfig, rx, tx, [0xA241])




#Test for: setModuleConfig()
rx = struct.pack("<BHB", 0x17, 5, int(True))
tx = struct.pack("<BHB", 0x17, 5, 0x01)
testFunction(lib.setModuleConfig, rx, tx, [5, False, True])




#Test for: startDataStream()
rx = struct.pack("<B", 0x20)
tx = struct.pack("<BH", 0x20, 0xFFFF)
testFunction(lib.startDataStream, rx, tx)




#Test for: dataStreamIsRunning()
testFunction(lib.dataStreamIsRunning, None, None)




#Test for: readDataStream()
dataList = list()
for i in range(0, 100):
    rx = struct.pack("<IHBI", int(time.time() + i), i % 5, 5, i * 1234)
    lib._protocol.setAvailableData(rx)
    data = lib.readDataStream()
    if(data != None):
        dataList.append(data)

print("Data stream list:" + str(dataList) + "\n")
        



#Test for: stopDataStream()
rx = struct.pack("<B", 0x21)
tx = struct.pack("<B", 0x21)
testFunction(lib.stopDataStream, rx, tx)




#Test for: dataStreamIsRunning()
testFunction(lib.dataStreamIsRunning, None, None)




#Test for: getDeviceInformation()
testFunction(lib.getDeviceInformation, None, None)




#Throws an exception because instruction ID does not match (only works before executing other functions due to caching)
#rx = struct.pack("B11s11s8sBH", 254, b'1.2.3.4.5.6', b'Jan 01 2020', b'14:30:05', 5, 267)
#try:
#    testFunction(lib.getDeviceInformation, rx, b'\xFF')
#except:
#    print("An intended exception occured. Instruction ID check passed")
