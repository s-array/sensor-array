#include "instructionhandler.h"
#include "config.h"
#include "eventlog.h"
#include "alarm.h"

#include <string.h>
#include <iostream>
using namespace std;

int instructionhandler_tests_passed = 0;
int instructionhandler_tests_total = 0;

void tx_buffer_matches(InstructionHandler *instr, uint8_t *buff){
	instructionhandler_tests_total++;

	int diff = 0;
	for(int i = 0; i < instr->_txBufferSize; i++){
		if(buff[i] != instr->_txBuffer[i]){
			diff++;
		}
	}
	
	if(diff == 0){
		cout << "TX buffer matches. Test passed.\n\n";
		instructionhandler_tests_passed++;
	}else{
		cout << "TX buffer does not match. Test failed.\n\n";
	}
}

void instructionhandler_unittest(){
	cout << "========== InstructionHandler Unit Test ==========\n";

	RTC rtc = RTC();
	StorageHandler storage = StorageHandler();
	Config config = Config(&storage);
	EventLog eventLog = EventLog(&storage, &rtc);
	ZigbeeHandler zigbee = ZigbeeHandler(&rtc);
	ModuleHandler moduleHandler = ModuleHandler(&storage, &config, &zigbee);
	InstructionHandler i = InstructionHandler(&config, &eventLog, &moduleHandler, &rtc);


	
	//Restore factory defaults
	config.restoreFactoryDefaults();
	moduleHandler.restoreFactoryDefaults();
	eventLog.restoreFactoryDefaults();
	
	//Add modules for testing
	uint8_t data = 0xFF;
	storage.write(ADDR_MODULES_BASE + 1, &data, sizeof(data));

	data = 0b10101101;
	storage.write(ADDR_MODULES_BASE + 0x05, &data, sizeof(data));

	uint32_t timestamp;
	//Add some events to the log
	for(int i = 0; i < 10; i++){
		eventLog.logEvent(i, i);
	}
	timestamp = rtc.getTimestamp();

	
	uint8_t buff_rx_getEvents[] = {0x01};
	uint8_t buff_tx_getEvents[] = {0x9, 0x0, 0x9, 0x0, 0x00, 0x00, 0x00, 0x00, 0x9, 0x0, 0x0, 0x0};
	memcpy(&buff_tx_getEvents[4], &timestamp, sizeof(timestamp));

	uint8_t buff_rx_deleteEvent[] = {0x02, 0x05, 0x00};
	uint8_t buff_tx_deleteEvent[] = {0x2, 0x5, 0x0};

	uint8_t buff_rx_deleteAllEvents[] = {0x02, 0xFF, 0xFF};
	uint8_t buff_tx_deleteAllEvents[] = {0x2, 0xff, 0xff};

	uint8_t buff_rx_getModulesList[] = {0x03};
	uint8_t buff_tx_getModulesList[] = {0x05, 0x00};

	uint8_t buff_rx_setConfigTime[] = {0x11, 0xFF, 0x00, 0x00, 0x00};
	uint8_t buff_tx_setConfigTime[] = {0x11};
	
	uint8_t buff_rx_getConfigTime[] = {0x10};
	uint8_t buff_tx_getConfigTime[] = {0x10, 0xFF, 0x00, 0x00, 0x00};

	uint8_t buff_rx_setAlarmConfig[] = {0x13, 0x03};
	uint8_t buff_tx_setAlarmConfig[] = {0x13};

	uint8_t buff_rx_getAlarmConfig[] = {0x12};
	uint8_t buff_tx_getAlarmConfig[] = {0x12, 0x03};

	uint8_t buff_rx_setSensorThreshold[] = {0x15, 0x05, 0xd5, 0xdd, 0x00, 0x00};
	uint8_t buff_tx_setSensorThreshold[] = {0x15};

	uint8_t buff_rx_getSensorThreshold[] = {0x14, 0x05};
	uint8_t buff_tx_getSensorThreshold[] = {0x14, 0xd5, 0xdd, 0x0, 0x0};

	uint8_t buff_rx_setModuleConfig[] = {0x17, 0x05, 0x00, 0x02};
	uint8_t buff_tx_setModuleConfig[] = {0x17, 0x05, 0x00, 0x01};

	uint8_t buff_rx_getModuleConfig[] = {0x16, 0x05, 0x00};
	uint8_t buff_tx_getModuleConfig[] = {0x16, 0x05, 0x00, 0x02, 0xA};

	uint8_t buff_rx_startStream[] = {0x20, 0xFF, 0xFF};
	uint8_t buff_tx_startStream[] = {0x20};

	uint8_t buff_rx_stopStream[] = {0x21};
	uint8_t buff_tx_stopStream[] = {0x21};

	uint8_t buff_tx_factoryReset[] = {0xFE};
	uint8_t buff_rx_factoryReset[] = {0xFE};

	uint8_t buff_rx_getDeviceInformation[] = {0xFF};
	//TX buffer is not checked because the content depends on the compile time
	
	memcpy(&i._rxBuffer, buff_rx_getEvents, sizeof(buff_rx_getEvents));
	i._rxByteCount =  sizeof(buff_rx_getEvents);
	i.executeInstruction();
	tx_buffer_matches(&i, buff_tx_getEvents);

	memcpy(&i._rxBuffer, buff_rx_deleteEvent, sizeof(buff_rx_deleteEvent));
	i._rxByteCount =  sizeof(buff_rx_deleteEvent);
	i.executeInstruction();
	tx_buffer_matches(&i, buff_tx_deleteEvent);

	memcpy(&i._rxBuffer, buff_rx_deleteAllEvents, sizeof(buff_rx_deleteAllEvents));
	i._rxByteCount =  sizeof(buff_rx_deleteAllEvents);
	i.executeInstruction();
	tx_buffer_matches(&i, buff_tx_deleteAllEvents);

	memcpy(&i._rxBuffer, buff_rx_getModulesList, sizeof(buff_rx_getModulesList));
	i._rxByteCount = sizeof(buff_rx_getModulesList);
	i.executeInstruction();
	tx_buffer_matches(&i, buff_tx_getModulesList);
	
	memcpy(&i._rxBuffer, buff_rx_setConfigTime, sizeof(buff_rx_setConfigTime));
	i._rxByteCount = sizeof(buff_rx_setConfigTime);
	i.executeInstruction();
	tx_buffer_matches(&i, buff_tx_setConfigTime);

	memcpy(&i._rxBuffer, buff_rx_getConfigTime, sizeof(buff_rx_getConfigTime));
	i._rxByteCount =  sizeof(buff_rx_getConfigTime);
	i.executeInstruction();
	tx_buffer_matches(&i, buff_tx_getConfigTime);

	memcpy(&i._rxBuffer, buff_rx_setAlarmConfig, sizeof(buff_rx_setAlarmConfig));
	i._rxByteCount =  sizeof(buff_rx_setAlarmConfig);
	i.executeInstruction();
	tx_buffer_matches(&i, buff_tx_setAlarmConfig);

	memcpy(&i._rxBuffer, buff_rx_getAlarmConfig, sizeof(buff_rx_getAlarmConfig));
	i._rxByteCount =  sizeof(buff_rx_getAlarmConfig);
	i.executeInstruction();
	tx_buffer_matches(&i, buff_tx_getAlarmConfig);
	
	memcpy(&i._rxBuffer, buff_rx_setSensorThreshold, sizeof(buff_rx_setSensorThreshold));
	i._rxByteCount =  sizeof(buff_rx_setSensorThreshold);
	i.executeInstruction();
	tx_buffer_matches(&i, buff_tx_setSensorThreshold);

	memcpy(&i._rxBuffer, buff_rx_getSensorThreshold, sizeof(buff_rx_getSensorThreshold));
	i._rxByteCount =  sizeof(buff_rx_getSensorThreshold);
	i.executeInstruction();
	tx_buffer_matches(&i, buff_tx_getSensorThreshold);
	
	memcpy(&i._rxBuffer, buff_rx_setModuleConfig, sizeof(buff_rx_setModuleConfig));
	i._rxByteCount =  sizeof(buff_rx_setModuleConfig);
	i.executeInstruction();
	tx_buffer_matches(&i, buff_tx_setModuleConfig);

	memcpy(&i._rxBuffer, buff_rx_getModuleConfig, sizeof(buff_rx_getModuleConfig));
	i._rxByteCount =  sizeof(buff_rx_getModuleConfig);
	i.executeInstruction();
	tx_buffer_matches(&i, buff_tx_getModuleConfig);

	memcpy(&i._rxBuffer, buff_rx_factoryReset, sizeof(buff_rx_factoryReset));
	i._rxByteCount = sizeof(buff_rx_factoryReset);
	i.executeInstruction();
	tx_buffer_matches(&i, buff_tx_factoryReset);

	memcpy(&i._rxBuffer, buff_rx_getDeviceInformation, sizeof(buff_rx_getDeviceInformation));
	i._rxByteCount =  sizeof(buff_rx_getDeviceInformation);
	i.executeInstruction();
	//TX buffer is not checked
	

	memcpy(&i._rxBuffer, buff_rx_startStream, sizeof(buff_rx_startStream));
	i._rxByteCount =  sizeof(buff_rx_startStream);
	i.executeInstruction();
	tx_buffer_matches(&i, buff_tx_startStream);

	memcpy(&i._rxBuffer, buff_rx_stopStream, sizeof(buff_rx_stopStream));
	i._rxByteCount =  sizeof(buff_rx_stopStream);
	i.executeInstruction();
	//TX buffer not checked, because the stream cannot be started without another thread

	cout << instructionhandler_tests_passed << " of " << instructionhandler_tests_total << " passed.\n";
}

void config_unittest(){
	cout << "========== Config Unit Test ==========\n";
	int passedTests = 0;	

	StorageHandler storage = StorageHandler();
	Config conf = Config(&storage);
	
	SensorType t;
	uint32_t value;
	uint32_t result;
	AlarmConfig ac;


	t = MQ135;
	value = 123456;
	result = conf.setThreshold(t, value);
	if(result){
		cout << "conf.setThreshold() result: " << result << "\n";
		passedTests++;
	}

	result = conf.getThreshold(t);
	if(result == value){
		cout << "conf.getThreshold() result: " << result << "\n";
		passedTests++;
	}

	t = MQ9;
	value = 789;
	result = conf.setThreshold(t, value);
	if(result){
		cout << "conf.setThreshold() result: " << result << "\n";
		passedTests++;
	}

	result = conf.getThreshold(t);
	if(result == value){
		cout << "conf.getThreshold() result: " << result << "\n";
		passedTests++;
	}
	

	ac.ledEnabled = true;
	ac.buzzerEnabled = true;
	result = conf.setAlarmConfig(ac);
	if(result){
		cout << "conf.setAlarmConfig() result: " << result << "\n";
		passedTests++;
	}
	
	ac = conf.getAlarmConfig();
	if(ac.ledEnabled && ac.buzzerEnabled){
		cout << "conf.getAlarmConfig() returned: {ledEnabled: " << ac.ledEnabled << ", buzzerEnabled: " << ac.buzzerEnabled << "}\n";
		passedTests++;
	}

	cout << passedTests << " of 6 passed.\n";
}

void eventlog_unittest(){
	cout << "========== EventLog Unit Test ==========\n";
	int passedTests = 0;

	StorageHandler storage = StorageHandler();
	RTC rtc = RTC();
	EventLog log = EventLog(&storage, &rtc);

	uint16_t eventCount = 0;
	Event e;
	


	//Init
	log.deleteEvent(0xFFFF);
	
	//Log 250 events
	for(int i = 0; i < 250; i++){
		log.logEvent(i, i);
	}

	eventCount = log.getEventCount();
	if(eventCount == 250){
		passedTests++;	
	}
	
	//Delete a few items
	log.deleteEvent(249);	//Delete last item
	log.deleteEvent(0);		//Delete first item
	log.deleteEvent(0);		//Delete first item(index 1)
	log.deleteEvent(100);	//Delete event number 100
	log.deleteEvent(1);		//Delete event number 1

	eventCount = log.getEventCount();
	if(eventCount == 245){
		passedTests++;
	}

	//Add one item
	log.logEvent(0xAAAA, 100);

	eventCount = log.getEventCount();
	if(eventCount == 246){
		passedTests++;
	}
	
	//Get the first item
	log.resetIndex();
	e = log.getEvent();

	if(e.moduleId == 2){
		passedTests++;
	}

	
	//Loop to the last event
	while(log.nextEvent()){}
	
	e = log.getEvent();
	if(e.moduleId == 0xAAAA){
		passedTests++;
	}

	cout << passedTests << " of 5 passed.\n";
}

void modulehandler_unittest(){
	cout << "\n========== ModuleHandler Unit Test ==========\n";

	int passedTests = 0;

	StorageHandler storage = StorageHandler();
	Config config = Config(&storage);
	RTC rtc = RTC();
	ZigbeeHandler zigbee = ZigbeeHandler(&rtc);
	ModuleHandler moduleHandler = ModuleHandler(&storage, &config, &zigbee);
	Module m;
	int moduleCount = 5;

	if(!moduleHandler.moduleIsKnown(0)){
		passedTests++;
	}

	if(moduleHandler.getModuleCount() == 0){
		passedTests++;
	}
	
	m.id = 0;
	m.alarmConfig = 0x03;
	m.sensorType = (SensorType) 0x00;		//Only used for testing, type is not updated with this function
	if(moduleHandler.updateModule(m)){
		passedTests++;
	}

	if(moduleHandler.moduleIsKnown(0)){
		passedTests++;
	}

	
	Module newModule = moduleHandler.getModule(m.id);
	if((newModule.id != m.id) || (newModule.sensorType != m.sensorType) || (newModule.alarmConfig.to_uint8() != m.alarmConfig.to_uint8())){
		cout << "Module received does not equal the stored module.\n";
		cout << newModule.id << ", " << hex << (int)newModule.sensorType << ", " << (int)newModule.alarmConfig.to_uint8() << dec << "\n";
	}else{
		passedTests++;
	}
	


	if(moduleHandler.deleteModule(0)){
		passedTests++;
	}

	//Add some modules
	for(int i = 0; i < moduleCount; i++){
		m.id = i;
		m.alarmConfig = 0x03;
		m.sensorType = MQ9;

		moduleHandler.updateModule(m);
	}


	//Get module count
	//instructionhandler_unittest();
	if(moduleHandler.getModuleCount() == moduleCount){
		passedTests++;
	}

	cout << passedTests << " of 7 passed.\n";
}

void alarm_unittest(){
	cout << "\n========== Alarm Unit Test ==========\n";
	int passedTests = 0;
	
	StorageHandler storage = StorageHandler();
	Config config = Config(&storage);
	Alarm alarm = Alarm(0,0,0,0, &config);

	//Simulate a run for 5 seconds
	alarm.start();
	for(int i = 0; i < 500; i++){
		alarm.tick();	
	}
	alarm.stop();

	//No functions can be tested here. Test can only be run on real hardware.
}

void unittest_all(){
	instructionhandler_unittest();
	config_unittest();
	eventlog_unittest();
	modulehandler_unittest();
	alarm_unittest();
}
