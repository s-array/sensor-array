#include "modulehandler.h"
#include "zigbeepackets.h"

#include <string.h>
#include <iostream>
#include <cerrno>
using namespace std;

#define MH_QUEUE_SIZE 16
#define MH_MSG_SIZE sizeof(ModuleHandlerMsg)

ModuleHandler::ModuleHandler(StorageHandler *storage, Config *config, ZigbeeHandler *zigbee):
	_storage(storage),
	_config(config),
	_zigbee(zigbee)
{
	
	this->_streamState = StreamState::UNKNOWN;
	
	memset(this->_localSoftwareVersion, 0, 11);
	strcpy(this->_localSoftwareVersion, SOFTWARE_VERSION);
	
	//Set module id
	this->_streamModuleId = MODULE_ID_MAX;

	//Set the message queue attributes
	this->_mqAttrs.mq_maxmsg = MH_QUEUE_SIZE;
	this->_mqAttrs.mq_msgsize = MH_MSG_SIZE;
	this->_mqAttrs.mq_flags = 0;

	//Open the queue
    this->_rfQueue = mq_open("/mh", O_RDWR | O_CREAT, 0666, &this->_mqAttrs);
	if(this->_rfQueue < 0){
		cout << "ERROR: Could not open message queue.\n";
        cout << strerror(this->_rfQueue) << "\n";
	}
}

ModuleHandler::~ModuleHandler(){
	mq_close(this->_rfQueue);
	//TODO
}

Module ModuleHandler::getModule(uint16_t id){
	Module m;
	m.id = id;

	uint8_t data = this->_readModuleData(id);

	m.sensorType = (SensorType) ((data & 0xF0) >> 4);
	m.alarmConfig = AlarmConfig((data & 0x0C) >> 2);

	return m;
}

bool ModuleHandler::moduleIsKnown(uint16_t id){
	//Read the module config
	uint8_t data = this->_readModuleData(id);

	//Check if the used bit is set
	return (data & 0x01);
}

uint16_t ModuleHandler::getModuleCount(){
	uint16_t count = 0;
	for(uint16_t i = 0; i <= MODULE_ID_MAX; i++){
		if(this->moduleIsKnown(i)){
			count++;
		}
	}
	return count;
}

bool ModuleHandler::updateModule(Module m){
	if(!this->_writeModule(m, true)){
		return false;
	}
	
	//Sync the module on the other thread
	ModuleHandlerMsg msg;
	msg.type = ModuleHandlerMsg::SyncModule;
	msg.moduleId = m.id;
	this->_addToQueue(msg);

	return true;
}

bool ModuleHandler::deleteModule(uint16_t id){
	Module m;
	m.id = id;
	m.sensorType = UNKNOWN;
	m.alarmConfig = (AlarmConfig)0x00;

	return this->_writeModule(m, false);
}

void ModuleHandler::syncAllModules(){
	//Sync all modules on the other thread
	ModuleHandlerMsg msg;
	msg.type = ModuleHandlerMsg::SyncModule;
	msg.moduleId = MODULE_ID_MAX;
	this->_addToQueue(msg);
}

bool ModuleHandler::setAlarmConfig(uint16_t id, AlarmConfig conf){
	Module m;
	
	//Check if the device is known
	if(!this->moduleIsKnown(id)){
		return false;
	}
	
	//Get the module and update the alarm config
	m = this->getModule(id);
	m.alarmConfig = conf;
	
	//Update the module
	return this->updateModule(m);
}

StreamState ModuleHandler::getStreamState(){
	return this->_streamState;
}

bool ModuleHandler::startStream(uint16_t id){
	ModuleHandlerMsg msg;
	msg.type = ModuleHandlerMsg::StartStream;

	//Check if stream can be started
	if(this->_streamState == StreamState::START_QUEUED || this->_streamState == StreamState::RUNNING || this->_streamState == StreamState::STOP_QUEUED){
		//Stream is either starting, running or stopping
		return false;
	}

	//Check if module is known 
	if((!this->moduleIsKnown(id)) && (id != MODULE_ID_ALL)){
		return false;
	}
	
	//Start the stream on the other thread
	msg.moduleId = id;
	this->_addToQueue(msg);

	//Set the state
	this->_streamState = StreamState::START_QUEUED;

	return true;
}

bool ModuleHandler::stopStream(){
	ModuleHandlerMsg msg;

	//Check if stream can be stopped
	if(this->_streamState == StreamState::START_QUEUED || this->_streamState == StreamState::NOT_RUNNING || this->_streamState == StreamState::STOP_QUEUED){
		//Stream is eieth not running, starting or stopping 
		return false;
	}

	//Stop the stream on the other thread
	msg.type = ModuleHandlerMsg::StopStream;
	msg.moduleId = this->_streamModuleId;
	this->_addToQueue(msg);
	
	//Set the state
	this->_streamState = StreamState::STOP_QUEUED;

	return true;
}

bool ModuleHandler::getStreamData(uint16_t *id, uint32_t *value, uint8_t *type){
	if(!this->_zigbee->getStreamData(id, value)){
		return false;
	}
	
	Module m = this->getModule(*id);
	type = (uint8_t *)m.sensorType;

	return true;
}

void ModuleHandler::sendWirelessPackets(){
	ModuleHandlerMsg msg;
	bool result;

    if(mq_receive(this->_rfQueue, (char *)&msg, MH_MSG_SIZE, 0) == -1){
		cout << strerror(errno) << "\n";	
		return;
	}

	switch(msg.type){
		case ModuleHandlerMsg::StartStream:
			this->_streamModuleId = msg.moduleId;
			
			//Check if all modules should be streaming
			if(msg.moduleId == MODULE_ID_MAX){
				result = true;
				for(uint16_t i = 0; i <= MODULE_ID_MAX; i++){
					if(this->moduleIsKnown(i)){
						if(!this->_zigbee->startStream(i)){
							result = false;
						}
					}
				}
			}else{
				//Start the stream on only this id.
				result = this->_zigbee->startStream(msg.moduleId);
			}

			//Update the stream state
			this->_streamState = (result) ? StreamState::RUNNING : StreamState::UNKNOWN;

			break;

		case ModuleHandlerMsg::StopStream:
			//Check if all modules should be stopped
			if(msg.moduleId == MODULE_ID_MAX){
				result = true;
				for(uint16_t i = 0; i <= MODULE_ID_MAX; i++){
					if(this->moduleIsKnown(i)){
						if(!this->_zigbee->stopStream(i)){
							result = false;
						}
					}
				}
			}else{
				//Stop the stream only for this id.
				result = this->_zigbee->stopStream(msg.moduleId);
			}

			//Update the stream state
			this->_streamState = (result) ? StreamState::NOT_RUNNING : StreamState::UNKNOWN;

			break;

		case ModuleHandlerMsg::SyncModule:
			//Sync the module or modules
			if(msg.moduleId == MODULE_ID_MAX){
				this->_syncAllModules();
			}else{
				this->_syncModule(msg.moduleId);
			}
			break;

		default:
			break;
	}
}

bool ModuleHandler::restoreFactoryDefaults(){
	uint8_t data = 0;

	//Only bit 0 should be set to 0, but erasing all data is easier
	for(uint16_t i = 0; i <= MODULE_ID_MAX; i++){
		if(!this->_storage->write(ADDR_MODULES_BASE + i, &data, sizeof(data))){
			return false;
		}
	}

	return true;
}

uint8_t ModuleHandler::_readModuleData(uint16_t id){
	uint8_t data = 0;

	if(!this->_idIsInRange(id)){
		return data;
	}
	
	//Read the module data
	this->_storage->read(ADDR_MODULES_BASE + id, &data, sizeof(data));

	return data;
}

bool ModuleHandler::_writeModule(Module m, bool used){
	uint8_t data = 0;

	if(!this->_idIsInRange(m.id)){
		return false;
	}
	
	data |= used;
	data |= m.alarmConfig.to_uint8() << 2;
	data |= m.sensorType << 4;

	cout << "Write data: " << hex << (int)data << dec << "\n";

	return this->_storage->write(ADDR_MODULES_BASE + m.id, &data, sizeof(data));
}

bool ModuleHandler::_versionMatches(ModuleInformation mInfo){
	//Check if the version string matches
	for(int i = 0; i < 11; i++){
		if(mInfo.softwareVersion[i] != this->_localSoftwareVersion[i]){
			//String does not match
			return false;
		}
	}

	return true;
}

void ModuleHandler::_addToQueue(ModuleHandlerMsg msg){
#ifdef UNITTEST 
	mq_attr attr;
	if(mq_getattr(this->_rfQueue, &attr) < 0){
		cout << "[UNITTEST] ERROR: Could not retrieve message queue attributes.\n";
		return;
	}

	if(attr.mq_curmsgs == attr.mq_maxmsg){
		cout << "[UNITTEST] Message Queue full. Skipping 'mq_send' for unittest.\n";
		return;
	}
#endif
    mq_send(this->_rfQueue, (char *)&msg, sizeof(msg), 0);
}

bool ModuleHandler::_syncModule(uint16_t id){
	Module m;

	//Get module information
	ModuleInformation mInfo = this->_zigbee->getModuleInformation(id);
	
	//Check for version issues
	if(!this->_versionMatches(mInfo)){
		//Return false if the version does not match. No sync is possible
		return false;
	}
	
	//Read the module form memory
	m = this->getModule(id);
	
	//Store the type in the struct
	m.sensorType = (SensorType) mInfo.sensorType; 
	
	//Set threshold
	if(!this->_zigbee->setSensorThreshold(id, this->_config->getThreshold(m.sensorType))){
		return false;
	}
	
	//Set alarm config
	if(!this->_zigbee->setAlarmConfig(id, m.alarmConfig)){
		return false;
	}

	//Store the module struct with the updated sensorType
	this->_writeModule(m, true);
	
	//No problems: return true
	return true;
}

bool ModuleHandler::_syncAllModules(){
	bool success = true;

	for(uint16_t i = 0; i <= MODULE_ID_MAX; i++){
		if(this->moduleIsKnown(i)){
			if(!this->_syncModule(i)){
				success = false;
			}
		}
	}
	return success;
}

bool ModuleHandler::_idIsInRange(uint16_t id){
	return (id <= MODULE_ID_MAX);
}
