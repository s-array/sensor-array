#include "eventlog.h"

#ifdef DEBUG
#include <iostream>
using namespace std;
#endif

EventLog::EventLog(StorageHandler *storage, RTC *rtc): 
	_storage(storage),
	_rtc(rtc)
{
	//TODO
}

EventLog::~EventLog(){
	//TODO
}

bool EventLog::logEvent(uint16_t moduleId, uint32_t sensorValue){
	Event e;
	e.moduleId = moduleId;
	e.timestamp = this->_rtc->getTimestamp();
	e.value = sensorValue;

	return this->_addEvent(e);
}

bool EventLog::deleteEvent(uint16_t n){
	uint16_t prevEventIndex = 0;

	StoredEvent deletedEvent;
	StoredEvent prevEvent;

	//Check if all events should be deleted
	if(n == EVENTLOG_LINKEDLIST_NULLPTR){
		return this->_deleteAllEvents();
	}
	
	//Start at the beginning
	this->resetIndex();

	//If the index is a NULLPTR, there are no next events
	if(this->_eventIndex == EVENTLOG_LINKEDLIST_NULLPTR){
		return false;
	}
	
	if(n == 0){
		prevEventIndex = EVENTLOG_LINKEDLIST_NULLPTR;
	}else{
		//Search for the event n-1
		while(this->_eventNumber != n){
			prevEventIndex = this->_eventIndex;
			this->nextEvent();
		}
	}

	//Get both events
	prevEvent = this->_getEvent(prevEventIndex);
	deletedEvent = this->_getEvent(this->_eventIndex);

	//Update the deleted event with the available flag
	deletedEvent.timestamp = EVENTLOG_LINKEDLIST_AVAILABLE_MASK;

	//Update the next event ptr in the previous event
	prevEvent.nextEventPtr = deletedEvent.nextEventPtr;

	//Store the events and return
	return
	(
		this->_storeEvent(prevEventIndex, prevEvent)
		&&
		this->_storeEvent(this->_eventIndex, deletedEvent)
	);
}

void EventLog::resetIndex(){
	//Get the pointer to the first event in the list
	StoredEvent ptrEvent = this->_getEvent(EVENTLOG_LINKEDLIST_NULLPTR);
	this->_eventIndex = ptrEvent.nextEventPtr;

	//Reset the eventnumber
	this->_eventNumber = 0;
}

bool EventLog::nextEvent(){
	//Get the current event
	StoredEvent se = this->_getEvent(this->_eventIndex);

	//Check if the next event exists
	if(se.nextEventPtr != EVENTLOG_LINKEDLIST_NULLPTR){
		//Store the index of the next event
		this->_eventIndex = se.nextEventPtr;

		//Increment the eventnumber counter
		this->_eventNumber++;
		
		return true;
	}else{
		//Next event is not available
		return false;
	}
}

Event EventLog::getEvent(){
	//Get the event 
	StoredEvent se = this->_getEvent(this->_eventIndex);
	
	//Fill the event struct with data
	Event e;
	e.moduleId = se.moduleId;
	e.timestamp = se.timestamp;
	e.value = se.value;

	return e;
}

uint16_t EventLog::getEventNumber(){
	return this->_eventNumber;
}

uint16_t EventLog::getEventCount(){
	uint16_t eventCount = 0;
	uint16_t eventIndexTmp = this->_eventIndex;
	uint16_t eventNumberTmp = this->_eventNumber;
	
	//Start at the beginning
	this->resetIndex();

	//Loop through all events until the last event is found
	while(this->nextEvent()){}

	//If the last item is none
	if(this->_eventIndex == EVENTLOG_LINKEDLIST_NULLPTR){
		eventCount = 0;
	}else{
		eventCount = this->_eventNumber + 1;
	}

	//Restore the tmp values
	this->_eventIndex = eventIndexTmp;
	this->_eventNumber = eventNumberTmp;	
	
	return eventCount;
}

bool EventLog::restoreFactoryDefaults(){
	return this->_deleteAllEvents();
}

bool EventLog::_addEvent(Event e){
	StoredEvent newEvent;
	StoredEvent prevEvent;
	int32_t newEventIndex = this->_getEmptyEventIndex();
	
	//Check if space is available 
	if(newEventIndex == EVENTLOG_LINKEDLIST_NULLPTR){
		return false;
	}

	//Fill the new event struct with data
	newEvent.moduleId = e.moduleId;
	newEvent.timestamp = e.timestamp;
	newEvent.value = e.value;
	newEvent.nextEventPtr = EVENTLOG_LINKEDLIST_NULLPTR;
	
	//Find the last event
	this->resetIndex();
	while(this->nextEvent()){}
	prevEvent = this->_getEvent(this->_eventIndex);
	
	//Update the previous event with the new ptr
	prevEvent.nextEventPtr = newEventIndex; 
	
	//Store the events and return
	return
	(
		this->_storeEvent(newEventIndex, newEvent)
		&&
		this->_storeEvent(this->_eventIndex, prevEvent)
	);
}

bool EventLog::_deleteAllEvents(){
	StoredEvent e;
	e.timestamp = EVENTLOG_LINKEDLIST_AVAILABLE_MASK;
	e.nextEventPtr = EVENTLOG_LINKEDLIST_NULLPTR;

	//Write EVENTLOG_LINKEDLIST_AVAILABLE_MASK to all items 
	for(int i = EVENTLOG_LINKEDLIST_BEGIN; i <= EVENTLOG_LINKEDLIST_END; i++){
		//Calculate the address
		uint32_t address = (i * sizeof(e)) + sizeof(e.moduleId);

		//Write to the item
		if(!this->_storage->write(ADDR_EVENTLOG_BASE + address, &e.timestamp, sizeof(e.timestamp))){
			return false;
		}
	}
	
	//Store the event on index EVENTLOG_LINKEDLIST_NULLPTR
	return this->_storeEvent(EVENTLOG_LINKEDLIST_NULLPTR, e);
}

bool EventLog::_storeEvent(uint16_t index, StoredEvent e){
	return this->_storage->write(ADDR_EVENTLOG_BASE + (index * sizeof(e)), &e, sizeof(e));
}

StoredEvent EventLog::_getEvent(uint16_t index){
	StoredEvent e;
	this->_storage->read(ADDR_EVENTLOG_BASE + (index * sizeof(e)), &e, sizeof(e));
	
	//Return the StoredEvent struct
	return e;
}

uint16_t EventLog::_getEmptyEventIndex(){
	StoredEvent event;

	//Loop to EVENTLOG_LINKEDLIST_END
	for(uint16_t i = EVENTLOG_LINKEDLIST_BEGIN; i < EVENTLOG_LINKEDLIST_END; i++){
		event = this->_getEvent(i);

		if(event.timestamp == EVENTLOG_LINKEDLIST_AVAILABLE_MASK){
			//Return the index of the empty event
			return i;
		}
	}
	
	//No empty places found
	return EVENTLOG_LINKEDLIST_NULLPTR;
}

#ifdef DEBUG
void EventLog::debugPrint(){
	StoredEvent event;
	
	cout << "Eventlog: \n";
	for(int i = EVENTLOG_LINKEDLIST_BEGIN; i <= EVENTLOG_LINKEDLIST_NULLPTR; i++){
		event = this->_getEvent(i);
		cout << hex << i << ": " << event.moduleId << ", " << event.timestamp << ", " << event.value << ", " << event.nextEventPtr << dec << "\n";
	}
}
#endif
