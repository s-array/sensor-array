#include "instructionhandler.h"
#include "modulehandler.h"

//Threading
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds

//Cout etc.
#include <iostream>
#include <cstdlib>
using namespace std;


#define THREAD_UART_STACKSIZE 2048 
#define THREAD_UARTSTREAM_STACKSIZE 2048 
#define THREAD_WIRELESS_STACKSIZE 2048
#define THREAD_EVENTLOGHANDLER_STACKSIZE 2048

#ifdef UNITTEST
void unittest_all();
#endif

/**
 * \brief Parameters to be passed to a thread
 */
struct ThreadParameters{
	EventLog *eventLog;
	ZigbeeHandler *zigbee;
	ModuleHandler *moduleHandler;
	InstructionHandler *instructionHandler;
};

void *thread_UARTHandler(void *p){
    ThreadParameters *params = (ThreadParameters *)p;

	while(true){
		//Read blocking until data is available
		params->instructionHandler->readData();

		//Execute received instructions
		params->instructionHandler->executeInstruction();
	}
}

void *thread_UARTStream(void *p){
    ThreadParameters *params = (ThreadParameters *)p;

	while(true){
		//Check if stream is runnning
		if(params->moduleHandler->getStreamState() == StreamState::RUNNING){
			//Send stream data
			params->instructionHandler->sendStreamData();
		}
	}
}

void *thread_wirelessHandler(void *p){
    ThreadParameters *params = (ThreadParameters *)p;

	while(true){
		//Execute wireless functions from the queue
		params->moduleHandler->sendWirelessPackets();
	}
}

void *thread_eventlogHandler(void *p){
    ThreadParameters *params = (ThreadParameters *)p;

	uint16_t moduleId;
	uint32_t value;

	while(true){
		//Read and store threshold events
		if(params->zigbee->getThresholdData(&moduleId, &value)){
			//Data received store the event
			params->eventLog->logEvent(moduleId, value);
		}
	}
}


//TODO: REMOVE CODE FOR TESTING ONLY
void *thread_zigbeeTest(void *p){
    ThreadParameters *params = (ThreadParameters *)p;

	uint32_t loopCounter = 0;

	StreamData stream;
	stream.value = 0;

	ThresholdEvent ev;
	ev.value = 0;

	while(true){
		this_thread::sleep_for(chrono::milliseconds(rand() % 500));

		if(params->moduleHandler->getStreamState() == StreamState::RUNNING){
			memcpy(params->zigbee->_rxBuffer, &stream, sizeof(stream));

			params->zigbee->_rxFromModule = 1;
			params->zigbee->_rxBufferSize = sizeof(stream);
			params->zigbee->_rxISR();

			stream.value += 1;

		}else 
		if(params->moduleHandler->getStreamState() == StreamState::STOP_QUEUED){

			uint8_t moduleTXData[] = {0x21};
			params->zigbee->_rxFromModule = 1;
			params->zigbee->_rxBufferSize = 1;
			memcpy(params->zigbee->_rxBuffer, moduleTXData, 1);

			params->zigbee->_rxISR();

		}else if(params->moduleHandler->getStreamState() == StreamState::START_QUEUED){

			uint8_t moduleTXData[] = {0x20};
			params->zigbee->_rxFromModule = 1;
			params->zigbee->_rxBufferSize = 1;
			memcpy(params->zigbee->_rxBuffer, moduleTXData, 1);

			params->zigbee->_rxISR();
		}

		loopCounter++;
		if(loopCounter % 5 == 0){
			memcpy(params->zigbee->_rxBuffer, &ev, sizeof(ev));

			params->zigbee->_rxFromModule = loopCounter;
			params->zigbee->_rxBufferSize = sizeof(ev);
			params->zigbee->_rxISR();

			ev.value += 1;
		}
	}
}

int main(void){
#ifdef UNITTEST
	unittest_all();
	return 0;
#endif

	//Init the objects
	RTC rtc = RTC();
	StorageHandler storage = StorageHandler();
	Config config = Config(&storage);
	EventLog eventLog = EventLog(&storage, &rtc);
	ZigbeeHandler zigbee = ZigbeeHandler(&rtc);
	ModuleHandler moduleHandler = ModuleHandler(&storage, &config, &zigbee);
	InstructionHandler instructionHandler = InstructionHandler(&config, &eventLog, &moduleHandler, &rtc);

	//TODO: Remove the factory reset functions
	config.restoreFactoryDefaults();
	moduleHandler.restoreFactoryDefaults();
	eventLog.restoreFactoryDefaults();
	
	//TODO: Remove these test values
	uint8_t data = 0xFF;
	storage.write(ADDR_MODULES_BASE + 1, &data, sizeof(data));
	
	//Thread vars
	pthread_t t_uart, t_uart_stream, t_wireless, t_eventlog, t_zigbeeTest;
	pthread_attr_t uart_attr, uart_stream_attr, wireless_attr, eventlog_attr;
	
	//Init thread attributes
	pthread_attr_init(&uart_attr);
	pthread_attr_init(&uart_stream_attr);
	pthread_attr_init(&wireless_attr);
	pthread_attr_init(&eventlog_attr);

	//Stack size
	pthread_attr_setstacksize(&uart_attr, THREAD_UART_STACKSIZE);
	pthread_attr_setstacksize(&uart_stream_attr, THREAD_UARTSTREAM_STACKSIZE);
	pthread_attr_setstacksize(&wireless_attr, THREAD_WIRELESS_STACKSIZE);
	pthread_attr_setstacksize(&eventlog_attr, THREAD_EVENTLOGHANDLER_STACKSIZE);

	//Setup thread arguments
	ThreadParameters params;
	params.moduleHandler = &moduleHandler;
	params.instructionHandler = &instructionHandler;
	params.zigbee = &zigbee;
	params.eventLog = &eventLog;

	//Create the threads
	pthread_create(&t_uart, &uart_attr, &thread_UARTHandler, &params);
	pthread_setschedprio(t_uart, 4);

	pthread_create(&t_uart_stream, &uart_stream_attr, &thread_UARTStream, &params);
	pthread_setschedprio(t_uart_stream, 2);

	pthread_create(&t_wireless, &wireless_attr, &thread_wirelessHandler, &params);
	pthread_setschedprio(t_wireless, 5);

	pthread_create(&t_eventlog, &eventlog_attr, &thread_eventlogHandler, &params);
	pthread_setschedprio(t_eventlog, 3);
	
	//TODO: Remove test thread
	pthread_create(&t_zigbeeTest, &wireless_attr, &thread_zigbeeTest, &params);
	pthread_setschedprio(t_zigbeeTest, 1);
	
	//Sleep forever
	while(1){}

	return 0;
}
