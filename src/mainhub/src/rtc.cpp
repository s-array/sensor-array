#include "rtc.h"

//TODO: remove this and use the HAL
#include <time.h>

RTC::RTC(){
	//TODO: setup hardware
}

RTC::~RTC(){
	//TODO
}

uint32_t RTC::getTimestamp(){
#ifdef UNITTEST
	return this->_timestamp;
#else
	//TODO: use HAL
	time_t seconds;
	time(&seconds);
	return (uint32_t)seconds;
#endif
}

bool RTC::setTimestamp(uint32_t timestamp){
#ifdef UNITTEST
	this->_timestamp = timestamp;
	return true;
#endif

	//TODO
	return true;
}
