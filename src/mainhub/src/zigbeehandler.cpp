#include "zigbeehandler.h"
#include "zigbeepackets.h"

#ifdef DEBUG
#include <iostream>
using namespace std;
#endif


//Response data Message Queue
#define RX_RESPONSE_MQ_SIZE 8
#define RX_RESPONSE_MQ_NAME "/r_mq"

//Threshold Message Queue
#define RX_THRESHOLD_MQ_SIZE 32
#define RX_THRESHOLD_MQ_NAME "/t_mq"

//Stream data Message Queue
#define RX_STREAM_MQ_SIZE 32
#define RX_STREAM_MQ_NAME "/s_mq"

//TX retries before giving up
#define TX_RETRIES 3

//RX retries before attempting a new TX
#define RX_RETRIES 8

//RX Timeout in seconds
#define RX_TIMEOUT 2

ZigbeeHandler::ZigbeeHandler(RTC *rtc): 
	_rtc(rtc)
{
	//Setup all message queues
	this->_initMessageQueues();
}

ZigbeeHandler::~ZigbeeHandler(){
	//Clear all queues
	this->_clearQueue(this->_responseMq);
	this->_clearQueue(this->_thresholdMq);
	this->_clearQueue(this->_streamDataMq);

	//Close all message queues
	mq_close(this->_responseMq);
	mq_close(this->_thresholdMq);
	mq_close(this->_streamDataMq);
}

bool ZigbeeHandler::startStream(uint16_t moduleId){
	StartReadingData p;
	
	//Try to send the packet
	if(this->_trySendPacket(moduleId, &p, sizeof(p), nullptr)){
		//Clear the queue
		this->_clearQueue(this->_streamDataMq);

		return true;
	}
	return false;
}

bool ZigbeeHandler::stopStream(uint16_t moduleId){
	StopReadingData p;
	return this->_trySendPacket(moduleId, &p, sizeof(p), nullptr);
}

bool ZigbeeHandler::getStreamData(uint16_t *moduleId, uint32_t *value){
	ZigbeeRXStreamPacket rx;
	StreamData s;

	//Receive the data from the Message Queue
	if(!this->_receiveStreamPacket(&rx)){
		return false;
	}
	
	//Copy the data
	this->_extractZigbeeRXPacket(&rx, &s, moduleId);
	memcpy(value, &s.value, sizeof(s.value));

	return true;
}

bool ZigbeeHandler::getThresholdData(uint16_t *moduleId, uint32_t *value){
	ZigbeeRXThresholdPacket rx;
	ThresholdEvent t;
	
	//Receive the data from the Message Queue
	if(!this->_receiveThresholdPacket(&rx)){
		return false;
	}
	
	//Copy the data
	this->_extractZigbeeRXPacket(&rx, &t, moduleId);
	memcpy(value, &t.value, sizeof(t.value));

	return true;
}

ModuleInformation ZigbeeHandler::getModuleInformation(uint16_t moduleId){
	ModuleInformation mInfo;
	GetModuleInformation tx;
	ZigbeeRXPacket rx;

	//Try to send the packet and wait for a response
	if(!this->_trySendPacket(moduleId, &tx, sizeof(tx), &rx)){
		return mInfo;
	}
	
	//Copy the module information packet data to the struct and return
	this->_extractZigbeeRXPacket(&rx, &mInfo, nullptr);
	return mInfo;
}

bool ZigbeeHandler::setSensorThreshold(uint16_t moduleId, uint32_t value){
	SetModuleThreshold p;
	p.sensorThreshold = value;
	return this->_trySendPacket(moduleId, &p, sizeof(p), nullptr);
}

bool ZigbeeHandler::setAlarmConfig(uint16_t moduleId, AlarmConfig conf){
	SetModuleAlarmConfig p;
	p.alarmConfig = conf.to_uint8();
	return this->_trySendPacket(moduleId, &p, sizeof(p), nullptr);
}

bool ZigbeeHandler::_trySendPacket(uint16_t moduleId, ZigbeePacket *p, size_t size, ZigbeeRXPacket *rxPacket){
	ZigbeeTXPacket tx;
	ZigbeeRXPacket rx;
	int retries = 0;

	tx.moduleId = moduleId;
	tx.dataSize = size;
	memcpy(tx.data, p, size);

	while(retries != TX_RETRIES){
#ifdef DEBUG
		cout << "TX: try " << (retries+1) << "\n";
#endif

		//Send the packet
		this->_sendPacket(&tx);
		
		//Check if the device responded
		if(this->_tryReceiveResponsePacket(&rx, this->_getPacketId(&tx), tx.dataSize, moduleId)){
			if(rxPacket != nullptr){
				//Copy the received packet
				memcpy(rxPacket, &rx, sizeof(rx));
			}
			return true;
		}
		retries++;
	}
	
	//No response received
	return false;
}

//TODO: Send packet p to moduleId with zigbee lib
void ZigbeeHandler::_sendPacket(ZigbeeTXPacket *tx){
	uint8_t buff[tx->dataSize];
	memcpy(&buff, tx->data, tx->dataSize);
	
	cout << "[ZIGBEE] TX ModuleID: " << tx->moduleId << " data[" << int(tx->dataSize) << "]: {";
	for(int i = 0; i < (int)tx->dataSize; i++){
		cout << "0x" << hex << int((uint8_t)buff[i]) << dec << ", ";
	}
	cout << "}\n";
}

bool ZigbeeHandler::_receivePacket(ZigbeeRXPacket *rx, size_t size, mqd_t *mq, bool timeout){
	if(timeout){
		struct timespec tm;

		tm.tv_nsec = 0;
		tm.tv_sec = this->_rtc->getTimestamp();
		tm.tv_sec += RX_TIMEOUT;

		//return if the packet was received on time
		return (mq_timedreceive(*mq, (char *)rx, size, NULL, &tm) > 0);
	}else{
		return (mq_receive(*mq, (char *)rx, size, NULL) > 0);
	}
}

bool ZigbeeHandler::_tryReceiveResponsePacket(ZigbeeRXPacket *rx, uint8_t packetId, size_t packetSize, uint16_t moduleId){
	int retries = 0;
	while(retries != RX_RETRIES){
#ifdef DEBUG
		cout << "\nRX: try " << (retries+1) << "\n";
#endif
		//Block until data is received
		if(this->_receiveResponsePacket(rx)){
#ifdef DEBUG
		cout << "RX: Data received.\n";
#endif
			//Check if the packet is expected
			if(this->_isExpectedPacket(rx, packetId, packetSize, moduleId)){
#ifdef DEBUG
		cout << "RX: Expected packet received.\n";
#endif
				return true;
			}
		}
		retries++;
#ifdef DEBUG
		cout << "RX: Fail.\n";
#endif
	}
		
	//No matching packet was received after the amount of retries
	return false;
}

bool ZigbeeHandler::_receiveResponsePacket(ZigbeeRXPacket *rx){
	return this->_receivePacket(rx, sizeof(ZigbeeRXTXPacket), &this->_responseMq, true);
}

bool ZigbeeHandler::_receiveThresholdPacket(ZigbeeRXThresholdPacket *rx){
	return this->_receivePacket(rx, sizeof(ZigbeeRXThresholdPacket), &this->_thresholdMq, false);
}

bool ZigbeeHandler::_receiveStreamPacket(ZigbeeRXStreamPacket *rx){
	return this->_receivePacket(rx, sizeof(ZigbeeRXStreamPacket), &this->_streamDataMq, false);
}


//TODO: change this function to match real HAL behaviour
void ZigbeeHandler::_rxISR(void){
	ZigbeeRXPacket rx;
	
	//Copy the data to the temp buffer
	memcpy(rx.data, this->_rxBuffer, this->_rxBufferSize);
	rx.dataSize = this->_rxBufferSize;
	rx.moduleId = this->_rxFromModule;

	//Send the data to the corresponding message queue
	switch(this->_getPacketId(&rx)){
		case PACKETID_STREAMDATA:
			ZigbeeRXStreamPacket rxs;
			memcpy(&rxs, &rx, sizeof(rx));
			
			//Send to message queue
			this->_addToQueue(this->_streamDataMq, (char *)&rxs, sizeof(rxs));
			
			break;
		case PACKETID_THRESHOLD_EVENT:
			ZigbeeRXThresholdPacket rxt;
			memcpy(&rxt, &rx, sizeof(rx));
			
			//Send to message queue
			this->_addToQueue(this->_thresholdMq, (char *) &rxt, sizeof(rxt));

			break;
		default:		//All other received packets
			//Send to message queue
			this->_addToQueue(this->_responseMq, (char *) &rx, sizeof(rx));
			break;
	}
}

uint8_t ZigbeeHandler::_getPacketId(ZigbeeRXTXPacket *p){
	return (uint8_t) p->data[0];
}

bool ZigbeeHandler::_isExpectedPacket(ZigbeeRXPacket *p, uint8_t packetId, size_t packetSize, uint16_t moduleId){
	return ((p->moduleId == moduleId) && (this->_getPacketId(p) == packetId) && (p->dataSize == packetSize));
}

void ZigbeeHandler::_extractZigbeeRXPacket(ZigbeeRXPacket *rx, ZigbeePacket *p, uint16_t *moduleId){
	if(moduleId != nullptr){
		*moduleId = rx->moduleId;
	}
	memcpy(p, rx->data, rx->dataSize);
}


void ZigbeeHandler::_initMessageQueues(){
	//Response Message Queue
	this->_responseMqAttrs.mq_maxmsg = RX_RESPONSE_MQ_SIZE;
	this->_responseMqAttrs.mq_msgsize = sizeof(ZigbeeRXPacket);
	this->_responseMqAttrs.mq_flags = 0;
	
	//Threshold Message Queue
	this->_thresholdMqAttrs.mq_maxmsg = RX_THRESHOLD_MQ_SIZE;
	this->_thresholdMqAttrs.mq_msgsize = sizeof(ZigbeeRXThresholdPacket);
	this->_thresholdMqAttrs.mq_flags = 0;

	//Data stream Message Queue
	this->_streamDataMqAttrs.mq_maxmsg = RX_STREAM_MQ_SIZE;
	this->_streamDataMqAttrs.mq_msgsize = sizeof(ZigbeeRXStreamPacket);
	this->_streamDataMqAttrs.mq_flags = 0;
	
	//Setup the message queues
	this->_responseMq = mq_open(RX_RESPONSE_MQ_NAME, O_CREAT | O_RDWR, 0666, &this->_responseMqAttrs);
    this->_thresholdMq = mq_open(RX_THRESHOLD_MQ_NAME, O_CREAT | O_RDWR, 0666, &this->_thresholdMqAttrs);
    this->_streamDataMq = mq_open(RX_STREAM_MQ_NAME, O_CREAT | O_RDWR, 0666, &this->_streamDataMqAttrs);

	//Clear all queues
	this->_clearQueue(this->_responseMq);
	this->_clearQueue(this->_thresholdMq);
	this->_clearQueue(this->_streamDataMq);


#ifdef DEBUG
	if(this->_responseMq < 0){
		cout << "ERROR: Could not open 'responseMq' message queue.\n";
        cout << strerror(this->_responseMq) << "\n";
	}else if(this->_thresholdMq < 0){
		cout << "ERROR: Could not open 'thresholdMq' message queue.\n";
        cout << strerror(this->_thresholdMq) << "\n";
	}else if(this->_streamDataMq < 0){
		cout << "ERROR: Could not open 'streamDataMq' message queue.\n";
        cout << strerror(this->_streamDataMq) << "\n";
	}else{
		cout << "[ZIGBEE] All queues opened.\n";
	}
#endif
}

void ZigbeeHandler::_clearQueue(mqd_t &mq){
	mq_attr attr;

#ifdef DEBUG
	cout << "Clearing queue..\n";
#endif

	if(mq_getattr(mq, &attr) < 0){
		return;
	}
	uint8_t buff[attr.mq_msgsize];
	
	while(attr.mq_curmsgs > 0){
		if(mq_receive(mq, (char *)&buff, sizeof(buff), NULL) < 0){
#ifdef DEBUG
			cout << "ERROR: Could not receive msg.\n";
#endif
			return;
		}
		if(mq_getattr(mq, &attr) < 0){
#ifdef DEBUG
			cout << "ERROR: Could not read attrs.\n";
#endif
			return;
		}
	}
}

bool ZigbeeHandler::_addToQueue(mqd_t &mq, char *data, size_t size){
	mq_attr attr;

	if(mq_getattr(mq, &attr) < 0){
#ifdef DEBUG
		cout << "ERROR: Could not retrieve message queue attributes.\n";
#endif
		return false;
	}

	if(attr.mq_curmsgs == attr.mq_maxmsg){
#ifdef DEBUG
		cout << "ERROR: Message Queue full.\n";
#endif
		return false;
	}

	return (mq_send(mq, data, size, 0) > 0);
}
