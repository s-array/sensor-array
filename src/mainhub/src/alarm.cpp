#include "alarm.h"

#define LED_BLINK_PERIOD 5
#define BUZZER_FREQ_BIAS 250
#define BUZZER_FREQ_MULTIPLIER 10

#include <iostream>
using namespace std;

Alarm::Alarm(int buzzerPin, int ledRPin, int ledGPin, int ledBPin, Config *conf): 
	_buzzerPin(buzzerPin),
	_ledRPin(ledRPin),
	_ledGPin(ledGPin),
	_ledBPin(ledBPin),
	_config(conf)
{
	//Enable all alarms (Can be disabled by reading from the config)
	this->_alarmConfig = AlarmConfig(0xFF);
	this->_running = false;
}

void Alarm::loadConfig(){
	this->_alarmConfig = this->_config->getAlarmConfig();
}

void Alarm::start(){
	this->_running = true;
	this->_counter = 0;
	this->_ledState = false;
}

void Alarm::stop(){
	this->_running = false;

	//TODO: Disable both buzzer and led
}

//Expected tick every 10ms
void Alarm::tick(){
	if(!this->_running){
		return;
	}
	
	if(this->_alarmConfig.ledEnabled){
		if(this->_counter % 50 == 0){
			this->_ledState = !this->_ledState;
		}

		cout << "[ALARM] Led state: " << this->_ledState << "\n";

		//TODO: Set the led state 
	}

	if(this->_alarmConfig.buzzerEnabled){
		uint32_t buzzerFrequency = BUZZER_FREQ_BIAS + BUZZER_FREQ_MULTIPLIER * this->_counter;
		cout << "[ALARM] Buzzer frequency: " << buzzerFrequency << "\n";

		//TODO: Set buzzer frequency
	}
	
	this->_counter++;
}
