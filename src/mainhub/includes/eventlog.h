#ifndef EVENTLOG_H
#define EVENTLOG_H 

#include <cstdint>
#include "event.h"
#include "storagehandler.h"
#include "rtc.h"

#define EVENTLOG_LINKEDLIST_BEGIN 0x0000
#define EVENTLOG_LINKEDLIST_END 0xFFFE
#define EVENTLOG_LINKEDLIST_NULLPTR 0xFFFF
#define EVENTLOG_LINKEDLIST_AVAILABLE_MASK 0x00000000


/**
 * \brief A event entry stored in memory
 */
struct StoredEvent{
	uint16_t moduleId;
	uint32_t timestamp;
	uint32_t value;
	uint16_t nextEventPtr;
}__attribute__((packed));


/**
 * \brief Add, update, delete and read log entries
 */
class EventLog{
	public:
		/**
		 * \brief Constructor
		 *
		 * @param storage[in] Pointer to a StorageHandler object
		 * @param rtc[in] Pointer to a RTC object
		 */
		EventLog(StorageHandler *storage, RTC *rtc);

		/**
		 * \brief Deconstructor
		 */
		~EventLog();
		
		/**
		 * \brief Log an event
		 * @param moduleId[in] The module id to log the event for
		 * @param sensorValue[in] The value which should be stored
		 * @return Storing the event was successful
		 */
		bool logEvent(uint16_t moduleId, uint32_t sensorValue);

		/**
		 * \brief Delete an event
		 *
		 * @param n[in] The event (id) to delete
		 * @return Deleting the event was successful
		 */
		bool deleteEvent(uint16_t n);
		
		/**
		 * \brief Reset the linked list index
		 */
		void resetIndex();

		/**
		 * \brief Go to the next event in the linked list
		 *
		 * @return Next event exists
		 */
		bool nextEvent();

		/**
		 * \brief Get an event from the linked list 
		 *
		 * @return The event from the linked list
		 */
		Event getEvent();

		/**
		 * \brief Get the current event number(id)
		 *
		 * @return The current event id
		 */
		uint16_t getEventNumber();

		/**
		 * \brief Get the event count stored in the log
		 *
		 * @return The eventcount in the log
		 */
		uint16_t getEventCount();

		/**
		 * \brief Restore the factory defaults
		 *
		 * Should also be executed before the first run of the application to initialize the memory layout
		 * 
		 * @return Restoring defaults was successful
		 */
		bool restoreFactoryDefaults();

#ifdef DEBUG
		void debugPrint();
#endif
	private:
		/**
		 * \brief Add an event to the linked list
		 * 
		 * @param e[in] The event to add
		 * @return The event was added to the linked list
		 */
		bool _addEvent(Event e);

		/**
		 * \brief Delete all events from memory
		 *
		 * @return Deleting all events was successful
		 */
		bool _deleteAllEvents();

		/**
		 * \brief Store an event in memory
		 *
		 * @param index[in] The index where the event should be stored
		 * @param e[in] The to be stored event
		 * @return Storing the event was successful
		 */
		bool _storeEvent(uint16_t index, StoredEvent e);

		/**
		 * \brief Read an event from memory
		 *
		 * @param index[in] The index of the event to be read
		 * @return The event from memory
		 */
		StoredEvent _getEvent(uint16_t index);

		/**
		 * \brief Search for an overwritable/empty index
		 *
		 * @return The index of an overwritable/empty event
		 */
		uint16_t _getEmptyEventIndex();

		//Variables
		StorageHandler *_storage;
		RTC *_rtc;

		uint16_t _eventIndex;
		uint16_t _eventNumber;
};

#endif
