#ifndef ALARM_H 
#define ALARM_H 

#include <cstdint>
#include "config.h"
#include "alarmconfig.h"

/**
 * \brief Start and stop the alarm on the device
 */
class Alarm{
	public:
		/**
		 * \brief Constructor
		 *
		 * @param buzzerPin[in] The pin on which the buzzer is connected
		 * @param ledRPin[in] The pin on which the Red LED is connected
		 * @param ledGPin[in] The pin on which the Green LED is connected
		 * @param ledBPin[in] The pin on which the Blue LED is connected
		 * @param conf[in] Pointer to a Config object
		 */
		Alarm(int buzzerPin, int ledRPin, int ledGPin, int ledBPin, Config *conf);
		
		/**
		 * \brief Load the configuration from the config in memory
		 */
		void loadConfig();
	
		/**
		 * \brief Start the alarm
		 */
		void start();

		/**
		 * \brief Stop the alarm
		 */
		void stop();

		/**
		 * \brief Tick the alarm
		 *
		 * Use Timer IRQ or threaded while loop with sleep
		 */
		void tick();

	private:
		int _buzzerPin;
		int _ledRPin;
		int _ledGPin;
		int _ledBPin;

		bool _running;
		uint8_t _counter;

		bool _ledState;

		AlarmConfig _alarmConfig;
		Config *_config;
};

#endif
