#ifndef ZIGBEE_PACKETS_H 
#define ZIGBEE_PACKETS_H

#include <cstdint>
#include <cstring>

#define PACKETID_MODULE_THRESHOLD 0x01
#define PACKETID_MODULE_ALARM_CONFIG 0x02
#define PACKETID_START_DATASTREAM 0x20
#define PACKETID_STOP_DATASTREAM 0x21
#define PACKETID_THRESHOLD_EVENT 0x22
#define PACKETID_STREAMDATA 0x23
#define PACKETID_DEVICE_INFORMATION 0xFF



//Pack all structs
#pragma pack(push, 1)

/**
 * \brief The ZigbeePacket base struct
 *
 * This packet can also be used as an ACK packet
 */
struct ZigbeePacket{
	uint8_t packet_id;
	ZigbeePacket(uint8_t id) : packet_id(id){};
};




/******************** Packet 0x01 ********************/
/**
 * \brief TX Packet to set the threshold of a module
 *
 */
struct SetModuleThreshold : ZigbeePacket{
	SetModuleThreshold() : ZigbeePacket(PACKETID_MODULE_THRESHOLD) {};
	uint32_t sensorThreshold;
};

/**
 * \brief RX Packet(ACK) to be received when the threshold of a module has been set
 */
struct SetModuleThresholdResponse : ZigbeePacket{
	SetModuleThresholdResponse() : ZigbeePacket(PACKETID_MODULE_THRESHOLD) {};
};




/******************** Packet 0x02 ********************/
/**
 * \brief TX Packet to set the alarm config of a module
 */
struct SetModuleAlarmConfig : ZigbeePacket{
	SetModuleAlarmConfig() : ZigbeePacket(PACKETID_MODULE_ALARM_CONFIG) {};
	uint8_t alarmConfig;
};

/**
 * \brief RX Packet(ACK) to be received when the alarm config of a module has been set
 */
struct SetModuleAlarmConfigResponse : ZigbeePacket{
	SetModuleAlarmConfigResponse() : ZigbeePacket(PACKETID_MODULE_ALARM_CONFIG) {};
};





/******************** Packet 0x20 ********************/
/**
 * \brief TX Packet to instruct the module to start a datastream
 *
 * Can also be used as RX Packet(ACK) when the stream will be started
 */
struct StartReadingData : ZigbeePacket{
	StartReadingData() : ZigbeePacket(PACKETID_START_DATASTREAM) {};
};




/******************** Packet 0x21 ********************/
/**
 * \brief TX Packet to instruct the module to stop the datastream
 *
 * Can also be used as RX Packet(ACK) when the stream will be stopped
 */
struct StopReadingData : ZigbeePacket{
	StopReadingData() : ZigbeePacket(PACKETID_STOP_DATASTREAM) {};
};



/******************** Packet 0x22 ********************/
/**
 * \brief RX Packet, received when the threshold of a module has been reached
 */
struct ThresholdEvent : ZigbeePacket{
	ThresholdEvent() : ZigbeePacket(PACKETID_THRESHOLD_EVENT) {};
	uint32_t value;
};

/**
 * \brief TX Packet to respond with when the threshold of a module has been reached
 */
struct ThresholdEventResponse : ZigbeePacket{
	ThresholdEventResponse() : ZigbeePacket(PACKETID_THRESHOLD_EVENT) {};
};


/******************** Packet 0x23 ********************/
/**
 * \brief RX Packet, received when one or more modules are streaming data 
 */
struct StreamData : ZigbeePacket{
	StreamData() : ZigbeePacket(PACKETID_STREAMDATA) {};
	uint32_t value;
};



/******************** Packet 0xFF ********************/
/**
 * \brief TX Packet to instruct a module to send information about itself
 */
struct GetModuleInformation : ZigbeePacket{
	GetModuleInformation() : ZigbeePacket(PACKETID_DEVICE_INFORMATION) {};
};

/**
 * \brief RX Packet, received when information has been requested from the module
 */
struct ModuleInformation : ZigbeePacket{
	//Constructor
	ModuleInformation() : ZigbeePacket(PACKETID_DEVICE_INFORMATION) {};
	char softwareVersion[11];	//This field is filled with init()
	uint8_t sensorType;			//This field must be filled manually
};


//Disable struct packing
#pragma pack(pop)
#endif
