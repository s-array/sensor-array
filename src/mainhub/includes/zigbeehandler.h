#ifndef ZIGBEE_PROTOCOL_HANDLER_H 
#define ZIGBEE_PROTOCOL_HANDLER_H 

#include <cstdint>
#include <cstring>

#include <fcntl.h>
#include <mqueue.h>

#include "zigbeepackets.h"
#include "alarmconfig.h"
#include "sensortype.h"
#include "rtc.h"

#define RXTX_BUFFERSIZE sizeof(ModuleInformation)
#define RX_THRESHOLD_DATASIZE sizeof(ThresholdEvent)
#define RX_STREAM_DATASIZE sizeof(StreamData)



/**
 * \brief Base struct with packet data and the module id for sending and receiving data.
 */
struct ZigbeeRXTXPacket{
	size_t dataSize;
	uint16_t moduleId;
	uint8_t data[RXTX_BUFFERSIZE];
};

/**
 * \brief Struct for TX packets
 */
struct ZigbeeTXPacket : ZigbeeRXTXPacket{};

/**
 * \brief Struct for RX packets
 */
struct ZigbeeRXPacket : ZigbeeRXTXPacket{};

/**
 * \brief Struct for RX threshold packets
 */
struct ZigbeeRXThresholdPacket : ZigbeeRXPacket{
	uint8_t data[RX_THRESHOLD_DATASIZE];
};

/**
 * \brief Struct for RX Stream data packets
 */
struct ZigbeeRXStreamPacket : ZigbeeRXPacket{
	uint8_t data[RX_STREAM_DATASIZE];
};


/**
 * \brief Zigbee protocol handler
 */
class ZigbeeHandler{
	public:
		//TODO: add zigbee obj
		/** \brief Constructor
		 *
		 * @param rtc[in] Pointer to an RTC object
		 */
		ZigbeeHandler(RTC *rtc);

		/**
		 * \brief Deconstructor
		 */
		~ZigbeeHandler();
	

		/**
		 * \brief Start a datastream for the specified moduleId
		 *
		 * @param moduleId[in] The module which should start a stream
		 * @return The start stream packet has been send
		 */
		bool startStream(uint16_t moduleId);

		/** 
		 * \brief Stop the stream for the specified moduleId
		 *
		 * @param moduleId[in] The module which should stop streaming
		 * @return The stop stream packet has been send
		 */
		bool stopStream(uint16_t moduleId);
		
		/**
		 * \brief Get data from the running stream
		 *
		 * This function is blocking until data has been read from the stream queue
		 *
		 * @param moduleId[out] The module id which has send the stream data
		 * @param data[out] The data in the stream packet
		 * @return Data could be read
		 */
		bool getStreamData(uint16_t *moduleId, uint32_t *data);

		/**
		 * \brief Get threshold data from the threshold data queue
		 *
		 * This function is blocking until data has been read from the threshold queue
		 *
		 * @param moduleId[out] The module id which has send the threshold packet 
		 * @param data[out] The value which the module has send in the packet.
		 * @return Data could be read
		 */
		bool getThresholdData(uint16_t *moduleId, uint32_t *data);
	
		/**
		 * \brief Get module information from the specified module id
		 *
		 * Sends a device information request to the module. Blocks until data is received or timeout.
		 *
		 * @param moduleId[in] The module to request the packet from 
		 * @return ModuleInformation Struct with information
		 */
		ModuleInformation getModuleInformation(uint16_t moduleId);
		
		/**
		 * \brief Update the sensor threshold value on the specified module
		 *
		 * Sends a sensor threshold update packet to the module
		 *
		 * @param moduleId[in] The module to send the packet to
		 * @param value[in] The new threshold value
		 * @return Sending data was successful
		 */
		bool setSensorThreshold(uint16_t moduleId, uint32_t value);

		/**
		 * \brief Update the alarm config on the specified module
		 *
		 * Sends a alarm config update packet to the module
		 * 
		 * @param moduleId[in] The module to send the packet to
		 * @param conf[in] The new alarm config
		 * @return Sending data was successful
		 */
		bool setAlarmConfig(uint16_t moduleId, AlarmConfig conf);
	

		//TODO: move to private
		/**
		 * \brief the ISR function to be called by the Zigbee hardware when new data has been received
		 */
		void _rxISR(void);

		//Testing variables:
		uint8_t _rxBuffer[RXTX_BUFFERSIZE];
		uint8_t _rxBufferSize;
		uint8_t _rxFromModule;

	private:
		/**
		 * \brief Attempt to send a packet and wait for a response
		 *
		 * The amount of TX retries is specified in TX_RETRIES
		 * The amount of RX retries is specified in RX_RETRIES
		 * The timeout for receiving new data is specified in RX_TIMEOUT
		 *
		 * @param moduleId[in] The module to send the packet to
		 * @param p[in] The packet to send
		 * @param size[in] The size of the packet in bytes
		 * @param rxPacket[out] The received response packet
		 * @return Sending data was successful
		 */
		bool _trySendPacket(uint16_t moduleId, ZigbeePacket *p, size_t size, ZigbeeRXPacket *rxPacket);

		/**
		 * \brief Send a packet without waiting for a response
		 *
		 * @param tx[in] The packet to send
		 * @return Sending data was successful
		 */
		void _sendPacket(ZigbeeTXPacket *tx);
	
		/**
		 * \brief Receive a response
		 * 
		 * Receive a response with the possibility to be blocking or timeout after some time
		 *
		 * @param rx[out] The received data packet
		 * @param size[in] The expected data size
		 * @param mq[in] The message queue to receive the packet from
		 * @param timeout[in] Timeout, or act as a blocking function
		 * @return Receiving data was successful
		 */
		bool _receivePacket(ZigbeeRXPacket *rx, size_t size, mqd_t *mq, bool timeout);

		/**
		 * \brief Try to receive a response from the response Message Queue. Will timeout if no data has been received in time
		 *
		 * @pararm rx[out] The received data packet
		 * @param packetId[in] The expected packet id
		 * @param packetSize[in] The expected packet size
		 * @param moduleId[in] The expected module id
		 * @return Receiving data was successful
		 */
		bool _tryReceiveResponsePacket(ZigbeeRXPacket *rx, uint8_t packetId, size_t packetSize, uint16_t moduleId);

		/**
		 * \brief Receive a response packet
		 *
		 * @param rx[out] The received response packet
		 * @return Receiving data was successful
		 */
		bool _receiveResponsePacket(ZigbeeRXPacket *rx);

		/**
		 * \brief Receive a threshold packet
		 *
		 * @param rx[out] The received threshold packet
		 * @return Receiving data was successful
		 */
		bool _receiveThresholdPacket(ZigbeeRXThresholdPacket *rx);

		/**
		 * \brief Receive a stream data packet
		 *
		 * @param rx[out] The received stream data packet
		 * @return Receiving data was successful
		 */
		bool _receiveStreamPacket(ZigbeeRXStreamPacket *rx);

		/**
		 * \brief Get the packet id from an ZigbeeRXTXPacket
		 *
		 * @param p[in] The ZigbeeRXTXPacket
		 * @return The packet id
		 */
		uint8_t _getPacketId(ZigbeeRXTXPacket *p);

		/**
		 * \brief Check if the packet is expected
		 *
		 * @param p[in] The ZigbeeRXPacket to check
		 * @param packetId[in] The exepected packet id
		 * @param packetSize[in] The expected size
		 * @param moduleId[in] The expected moduleId
		 * @return Packet is expected
		 */
		bool _isExpectedPacket(ZigbeeRXPacket *p, uint8_t packetId, size_t packetSize, uint16_t moduleId);

		/**
		 * \brief Extract a ZigbeeRXPacket to a ZigbeePacket and moduleId
		 *
		 * @param rx[in] The ZigbeeRXPacket to extract data from
		 * @param p[out] The output ZigbeePacket 
		 * @param moduleId[out] The output moduleId
		 */
		void _extractZigbeeRXPacket(ZigbeeRXPacket *rx, ZigbeePacket *p, uint16_t *moduleId);

		/**
		 * \brief Initialize the Message Queues
		 */
		void _initMessageQueues();

		/**
		 * \brief Clear a queue
		 *
		 * @param mq[in] The Message Queue to clear 
		 */
		void _clearQueue(mqd_t &mq);

		/**
		 * \brief Add data to a Message Queue
		 *
		 * If the queue is full, no data will be added to the queue.
		 *
		 * @param mq[in] The Message Queue to add data to
		 * @param data[in] The data to be added to the queue
		 * @param size[in] The size of the data to be addedS
		 * @return Data could be added to the queue
		 */
		bool _addToQueue(mqd_t &mq, char *data, size_t size);

		//Message Queue variables
		mq_attr _responseMqAttrs;
		mq_attr _thresholdMqAttrs;
		mq_attr _streamDataMqAttrs;

		mqd_t _responseMq;
		mqd_t _thresholdMq;
		mqd_t _streamDataMq;

		


		bool _streamRunning;
		
		//Objects
		RTC *_rtc;
		
};

#endif
