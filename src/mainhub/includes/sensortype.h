#ifndef SENSORTYPE_H
#define SENSORTYPE_H

#include <cstdint>

enum SensorType
{
	MQ2 = 0b0001,
	MQ3 = 0b0010,
	MQ4 = 0b0011,
	MQ5 = 0b0100,
	MQ6 = 0b0101,
	MQ7 = 0b0110,
	MQ8 = 0b0111,
	MQ9 = 0b1000,
	MQ135 = 0b1001,
	
	UNKNOWN = 0x0
};

#endif
