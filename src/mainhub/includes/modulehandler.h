#ifndef MODULEHANDLER_H 
#define MODULEHANDLER_H 

#include <cstdint>
#include <fcntl.h>    /* For O_* constants. */
#include <mqueue.h>

#include "storagehandler.h"
#include "config.h"
#include "zigbeehandler.h"
#include "alarmconfig.h"
#include "module.h"

#define MODULE_ID_MAX 0xFFFE
#define MODULE_ID_ALL 0xFFFF


/**
 * \brief Struct for Message Queue instructions
 */
struct ModuleHandlerMsg{
	enum {StartStream, StopStream, SyncModule} type;
	uint16_t moduleId;
};

/**
 * \brief Enum for possible stream states
 */
enum class StreamState {START_QUEUED, STOP_QUEUED, RUNNING, NOT_RUNNING, UNKNOWN};

/**
 * \brief Handler for all module related things
 */
class ModuleHandler{
	public:
		/**
		 * \brief Constructor
		 *
		 * @param storage[in] Pointer to a StorageHandler object
		 * @param config[in] Pointer to a Config object
		 * @param zigbee[in] Pointer to a ZigbeeHandler object
		 */
		ModuleHandler(StorageHandler *storage, Config *config, ZigbeeHandler *zigbee);

		/**
		 * \brief Deconstructor
		 */
		~ModuleHandler();
		
		/**
		 * \brief Get a Module from memory
		 *
		 * @param id[in] The id of the Module
		 * @return A Module struct
		 */
		Module getModule(uint16_t id);

		/**
		 * \brief Check if a module is known (stored in memory)
		 *
		 * @param id[in] The module id to check
		 * @return Module is known
		 */
		bool moduleIsKnown(uint16_t id);
		
		/**
		 * \brief Get the amount of modules stored in memory
		 *
		 * @return The amount of modules stored in memory
		 */
		uint16_t getModuleCount();
	
		/**
		 * \brief Update a module in memory
		 *
		 * @param m[in] The updated Module object
		 * @return Updating the module in memory was successful
		 */
		bool updateModule(Module m);
	
		/**
		 * \brief Delete the specified module from memory
		 * 
		 * @param id[in] The module id to be deleted
		 * @return Deleting the module from memory was successful
		 */
		bool deleteModule(uint16_t id);

		/**
		 * \brief Add Synchronize all modules to the queue
		 */
		void syncAllModules();
		
		/**
		 * \brief Store a AlarmConfig for the specified module
		 *
		 * @param id[in] The module id to send the new config to
		 * @param conf[in] The AlarmConfig to send to the module
		 * @return Storing the new AlarmConfig was successful
		 */
		bool setAlarmConfig(uint16_t id, AlarmConfig conf);
		
		/**
		 * \brief Get the current state of the stream
		 *
		 * @return The current StreamState
		 */
		StreamState getStreamState();

		/**
		 * \brief Add a start stream instruction to the queue
		 *
		 * @param id[in] The module that should start the stream
		 * @return Stream could be started
		 */
		bool startStream(uint16_t id);

		/**
		 * \brief Add a stop stream instruction to the Queue
		 *
		 * @return Stream could be stopped
		 */
		bool stopStream();

		/**
		 * \brief Read stream data from the ZigbeeHandler
		 *
		 * Blocking function. Should be executed on a thread.
		 *
		 * @param id[out] The module id from which the data has been received
		 * @param value[out] The value from the stream
		 * @param type[out] The SensorType of the module
		 * @return Data could be read
		 */
		bool getStreamData(uint16_t *id, uint32_t *value, uint8_t *type);
	
		/**
		 * \brief Send all instructions from the Message Queue as wireless packets 
		 *
		 * Blocking function. Should be executed on a thread.
		 */
		void sendWirelessPackets();

		/**
		 * \brief Restore the factory defaults
		 *
		 * Should also be executed before the first run of the application to initialize the memory layout
		 * 
		 * @return Restoring defaults was successful
		 */
		bool restoreFactoryDefaults();

	private:
		//Functions
		uint8_t _readModuleData(uint16_t id);
		bool _writeModule(Module m, bool used);
		bool _versionMatches(ModuleInformation mInfo);

		//Functions to be executed by handleQueue() 
		void _addToQueue(ModuleHandlerMsg msg);
		bool _syncModule(uint16_t id);
		bool _syncAllModules();

		bool _idIsInRange(uint16_t id);


		

	
		//Variables
		uint16_t _streamModuleId;
		StreamState _streamState;
		char _localSoftwareVersion[11];

		//Objects
		StorageHandler *_storage;
		Config *_config;
		ZigbeeHandler *_zigbee;
		
		//Message queue
		mq_attr _mqAttrs;
		mqd_t _rfQueue;
};

#endif
