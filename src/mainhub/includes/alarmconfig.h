#ifndef ALARMCONFIG_H 
#define ALARMCONFIG_H 

#include <cstdint>

/**
 * \brief Struct with alarm configuration data
 */
struct AlarmConfig{
	/**
	 * \brief Constructor without parameters
	 */
	AlarmConfig(){};

	/**
	 * \brief Constructor with parameter
	 * 
	 * @param conf[in] The config uint8_t to read 
	 */
	AlarmConfig(uint8_t conf){
		ledEnabled = conf & 0x01;
		buzzerEnabled = conf & 0x02;
	}
	
	/**
	 * \brief Convert the struct to an uint8_t
	 *
	 * @return Converted struct
	 */
	uint8_t to_uint8(){
		return (ledEnabled | buzzerEnabled << 1);
	}

	bool ledEnabled;
	bool buzzerEnabled;
};


#endif
