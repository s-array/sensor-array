#ifndef EVENT_H
#define EVENT_H 

#include <cstdint>

/**
 * \brief A logged event entry
 */
struct Event{
	uint16_t moduleId;
	uint32_t timestamp;
	uint32_t value;
};

#endif
