#ifndef CONFIG_H
#define CONFIG_H 

#include <cstdint>
#include "sensortype.h"
#include "alarmconfig.h"
#include "storagehandler.h"

class Config{
	public:
		Config(StorageHandler*);

		bool setThreshold(uint32_t value);
		uint32_t getThreshold();

		bool setAlarmConfig(AlarmConfig config);
		AlarmConfig getAlarmConfig();

		// restore default values
		bool restoreFactoryDefaults();
	private:
		StorageHandler *_storage;
};
#endif
