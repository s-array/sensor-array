#ifndef ALARMCONFIG_H 
#define ALARMCONFIG_H 

#include <cstdint>

struct AlarmConfig{
	// Constructors
	AlarmConfig(){};
	AlarmConfig(uint8_t conf){
		ledEnabled = conf & 0x01;
		buzzerEnabled = conf & 0x02;
	}

	uint8_t to_uint8(){
		return (ledEnabled | buzzerEnabled << 1);
	}

	// Fields
	bool ledEnabled;
	bool buzzerEnabled;
};

#endif
