#ifndef SENSOR_H 
#define SENSOR_H 

#include <cstdint>
#include <sensortype.h>
#include "config.h"

class Sensor{

	public:
		Sensor(Config *conf, int jumperPin0, int jumperPin1, int jumperPin2, 
			int jumperPin3, int dataPin);

		SensorType getSensorType();
		void setSensorType();
		uint32_t getSensorValue();
		bool hasExceededThresshold();

	private:
		int _jumperPin0, _jumperPin1, _jumperPin2, _jumperPin3, _dataPin;
		Config *_conf;
		// SensorType _sensorType;
		int _calcualteMQValue(int resistance, int dataPin);

};

#endif
