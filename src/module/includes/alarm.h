#ifndef ALARM_H 
#define ALARM_H 

#include <cstdint>
#include "config.h"
#include "alarmconfig.h"

class Alarm{
	public:
		Alarm(int buzzerPin, int ledRPin, int ledGPin, int ledBPin,Config *conf);
		void loadConfig();
		void start();
		void stop();
	private:
		int _buzzerPin;
		int _ledRPin;
		int _ledGPin;
		int _ledBPin;

		AlarmConfig _alarmConfig;
		Config *_config;
};

#endif
