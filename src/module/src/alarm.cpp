#include "alarm.h"

Alarm::Alarm(int buzzerPin, int ledRPin, int ledGPin, int ledBPin, Config *conf): 
	_buzzerPin(buzzerPin),
	_ledRPin(ledRPin),
	_ledGPin(ledGPin),
	_ledBPin(ledBPin),
	_config(conf)
{
	// TODO Wouter, wat gaat er aan?
	// //Enable all alarms (Can be disabled by reading from the config)
	// this->_alarmConfig = AlarmConfig(0xFF);
}

void Alarm::loadConfig(){
	// this->_alarmConfig = this->_config->getAlarmConfig();
}


void Alarm::start(){
	if(this->_alarmConfig.ledEnabled){
		//TODO: Enable the leds
	}

	if(this->_alarmConfig.buzzerEnabled){
		//TODO: Enable the buzzer
	}
}

void Alarm::stop(){
	//TODO: stop the alarm
}
