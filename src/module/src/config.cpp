#include "config.h"

#define ADDR_ALARM_BASE 0
#define ADDR_THRESHOLDS_BASE 1

Config::Config(StorageHandler *storage):
	_storage(storage)
{}

bool Config::setThreshold(uint32_t value){

	uint32_t addr = ADDR_THRESHOLDS_BASE;
	return this->_storage->write(addr, &value, sizeof(value));
}

uint32_t Config::getThreshold(){

	uint32_t value = 0;
	uint32_t addr = ADDR_THRESHOLDS_BASE;
	this->_storage->read(addr, &value, sizeof(value));
	return value;
}

bool Config::setAlarmConfig(AlarmConfig config){
	uint32_t addr = ADDR_ALARM_BASE;
	uint8_t value = config.to_uint8();
	return this->_storage->write(addr, &value, sizeof(value));
}

AlarmConfig Config::getAlarmConfig(){

	uint8_t data = 0;

	if(!this->_storage->read(ADDR_ALARM_BASE, &data, sizeof(data))){
		//Enable all alarms if reading is not possible
		data = 0xFF;
	}
	
	//Return the alarm config struct
	return AlarmConfig(data);
}

bool Config::restoreFactoryDefaults(){

	AlarmConfig ac;
	ac.ledEnabled = true;
	ac.buzzerEnabled = true;

	if(!this->setAlarmConfig(ac)){
		return false;
	}

	//Clear thresholds
	if(!this->setThreshold(0)){
		return false;
	}
	return true;
}
