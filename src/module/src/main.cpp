// Threading
#include <pthread.h>
#include <stdlib.h>
#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds

// Standaard
#include <iostream>
#include <cstdlib>

// Includes
#include "sensor.h"
#include "alarm.h"
#include "config.h"
using namespace std;

// TODO Wouter, waarom Stacksize?
#define THREAD_UART_STACKSIZE 2048 
#define THREAD_UARTSTREAM_STACKSIZE 2048 
#define THREAD_WIRELESS_STACKSIZE 2048

struct ThreadParameters{
	// ModuleHandler *moduleHandler;
	// InstructionHandler *instructionHandler;
	Sensor *sensor;
};

void *threadUART(void *p){

	ThreadParameters *params = (ThreadParameters *)p;

	while(true){
		//Read blocking until data is available
		// params->instructionHandler->readData();

		//Execute received instructions
		// params->instructionHandler->executeInstruction();

		// cout << params->sensor->getSensorValue() << "\n";
		// std::this_thread::sleep_for(std::chrono::seconds(1));
	}
}

void *threadUARTStream(void *p){

	while(true){
		// cout << "UARTStream\n";
		std::this_thread::sleep_for(std::chrono::seconds(0));
	}
}

void *threadWireless(void *p){

	while(true){
		// cout << "Wireless\n";
	}
}

int main(void){

	// Init objects
	StorageHandler storage = StorageHandler();
	Config config = Config(&storage);
	Sensor sensor = Sensor(&config,0,1,2,3,4);
	// Alarm alarm = Alarm(5,6,7, &config);

	// Thread ID vars
	pthread_t t_uart, t_uart_stream, t_wireless;
	pthread_attr_t uart_attr, uart_stream_attr, wireless_attr;

	//Init thread attributes
	pthread_attr_init(&uart_attr);
	pthread_attr_init(&uart_stream_attr);
	// pthread_attr_init(&wireless_attr);

	//Stack size
	pthread_attr_setstacksize(&uart_attr, THREAD_UART_STACKSIZE);
	pthread_attr_setstacksize(&uart_stream_attr, THREAD_UARTSTREAM_STACKSIZE);
	// pthread_attr_setstacksize(&wireless_attr, THREAD_WIRELESS_STACKSIZE);
	
	//Setup thread arguments
	ThreadParameters params;
	params.sensor = &sensor;
	// params.moduleHandler = &moduleHandler;
	// params.instructionHandler = &instructionHandler;
	// params.zigbee = &zigbee;

	//Create the threads
	pthread_create(&t_uart, &uart_attr, &threadUART, &params);
	pthread_setschedprio(t_uart, 4);

	pthread_create(&t_uart_stream, &uart_stream_attr, &threadUARTStream, &params);
	pthread_setschedprio(t_uart_stream, 2);

	// pthread_create(&t_wireless, &wireless_attr, &threadWireless, &params);
	// pthread_setschedprio(t_wireless, 3);

	//Sleep forever
	while(1){}

	return 0;
}
